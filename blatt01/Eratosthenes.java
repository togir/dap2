import java.util.Arrays;

public class Eratosthenes { 
    public static void main(String [] args){
        int a;
        boolean o = false;

        // ö

        if(args.length > 2 || args.length < 1) {
            throw new IllegalStateException(usageInfo());
        }

        try{
            a = Integer.parseInt(args[0]);
        } catch(Exception e){
            throw new IllegalStateException(usageInfo());
        }

        // Primzahlen sind nur für die natürlichen Zahlen definiert
        // deswegen ist eine Negative Eingabe nicht erlaubt.
        if(a < 0) {
            throw new IllegalStateException(usageInfo());
        }

        if(args.length == 2 && args[1].equals("-o")){
            o = true;
        }

        int[] zahlen = eratosthenes(a);

        System.out.println(zahlen.length);
        
        // Wenn -o erkannt wurde, dann sollen auch die Zahlen ausgegeben werden
        if(o) {
            for (int i : zahlen) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
        
    }

    public static int[] eratosthenes(int n){

        // Da 2 die erste Primzahl ist kann für eingaben kleiner als 2
        // ein leeres Array zureckegeben werden.
        if(n < 2) {
            return new int[0];
        }

        boolean[] isPrime = new boolean[n -1];
        int anzahl = 0;

        Arrays.fill(isPrime, true);

        for (int i = 2; i <= n; i++) {
            
            // Da der Index bei 2 gestartet wurde muss beim zugriff auf das Array der
            // Index wieder um 2 reduziert werden
            if(isPrime[i -2]) {
                anzahl++;

                int mult = 2;
                while(i * mult <= n) {

                    // Da der Index bei 2 gestartet wurde muss beim zugriff auf das Array der 
                    // Index wieder um 2 reduziert werden
                    isPrime[(i*mult) - 2] = false;
                    mult++;
                }
            }
        }
        

        int[] zahlen = new int[anzahl];
        // Extra counter für den Index, da die Arrays zahlen 
        // und isPrime nicht gleich lang sind.
        int indexZahlen = 0;

        for(int i = 0; i< isPrime.length; i++) {

            if(isPrime[i]) {
                zahlen[indexZahlen] = i+2;
                indexZahlen++;
            }
        }
        
        return zahlen;       
    }

    // Hilfsfunktion um die Bedienungsanleitung des Programmes zu generieren.
    private static String usageInfo()  {
        return "\n \nUsage: java ersatothenes n [-o] \n"
            + "Der Parameter n ist immer erforderlich und muss eine natürliche Zahl "
            + "darstellen.\n"
            + "Der Parameter -o ist optional. Er sorgt für eine Ausgabe der Primzahl(en) "
            + "von 0 bis n \n";
    }
}