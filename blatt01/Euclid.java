import java.security.InvalidParameterException;

class Euclid {
    public static void main(String[] args) {

        // Variablen für die Argumente die übergeben werden.
        int a, b;
        
        // Das Programm darf nach Aufgabenstellung nur mit 2 Parametern aufgerufen werden.
        if(args.length != 2) {
            throw new InvalidParameterException(
                "Das Programm benötigt 2 positive ganzzalige Argumente!"
                );
        }

        // Wenn ein Parameter keine Zahl ist gibt es einen Fehler, dieser wird abgefangen.
        try{
            a = Integer.parseInt(args[0]);
            b = Integer.parseInt(args[1]);
        } catch(NumberFormatException e) {
            throw new InvalidParameterException(
                "Das Programm benötigt 2 positive ganzzalige Argumente!"
                );
        }

        // Die Parameter dürfen nach Aufgabenstellung nicht negativ sein.
        if(a < 0 || b < 0) {
            throw new InvalidParameterException(
                "Das Programm benötigt 2 positive ganzzalige Argumente!"
                );
        }

        System.out.println(euclid(a, b));  
    }

    private static int euclid(int a, int b) {
        if(b == 0) return a;

        return euclid(b, a % b);
    }
}