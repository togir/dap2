
public class InsertionSort{
    public static void main( String[] args){

        // Nach Aufgabenstellung sind nur 2 Parameter erlaubt
        if(args.length > 2){
            System.out.println("FEHLER: Es wurden zu viele Parameter übergeben." + usageInfo());
            System.exit(1);
        }

        // Nach Aufgabenstellung muss es mindestens einen Paramter geben
        if( args.length < 1){
            System.out.println("FEHLER: Es muss mindestens die Länge des Feldes als Parameter  übergeben werden." + usageInfo());
            System.exit(1);
        }

        // Der erste Paramter muss eine natürliche Zahl sein.
        int a = 0;
        try{
            a = Integer.parseInt(args[0]);
        }
        catch(NumberFormatException e ){
            System.out.println("FEHLER: Der erste Parameter ist keine natürliche Zahl " + usageInfo());
            System.exit(1);
        }

        // Der erste Parameter muss größer als 1 sein, da sonst nicht sotiert werden kann.
        if(a < 1){
            System.out.println("FEHLER : Der erste Parameter ist nicht größer 1" + usageInfo());
            System.exit(1);
        }

        int[] array = new int [a];

        if(args.length == 2) {

            switch (args[1]) {
                case "rand": // Füllen des Arrays mit zufälligen Zahlen
                    fillArrayRand(array);
                    break;
                case "auf": // Füllen des Arrays mit mit aufsteigend Sortierten Zahlen
                    fillArrayAuf(array);
                    break;

                case "ab":
                    fillArrayAb(array); // Füllen des Arrays mit absteigend sortierten Zahlen
                    break;

                default:
                    System.out.println("FEHLER: Der zweite Parameter enthält eine unzulässige Zeichenkette!" + usageInfo());
                    System.exit(1);
                    break;
            }
        } else {
            fillArrayRand(array);  // Füllen des Arrays mit zufälligen Zahlen
        }
        
        // Nach Aufgabestellung soll das Array bei weniger als 100 Elemente ausgegeben werden.
        if (array.length < 100) printArray(array);
        
        
        
        // Variablen zur Zeitmessung
        long tStart, tEnd, msecs;
        tStart = System.currentTimeMillis();

        insertionSort(array);
        
        tEnd = System.currentTimeMillis();
        msecs = tEnd - tStart;
        
        if(isSorted(array)){
            
            // Nach Aufgabestellung soll das Array bei weniger als 100 Elemente ausgegeben werden.
            if(array.length < 100) printArray(array);
        
            System.out.println("Feld ist sortiert!");
            System.out.println ("Das Sortieren des Arrays hat " + msecs + " ms gedauert.");
        }
        else{
            System.out.println("FEHLER: Feld ist nicht sortiert!");
        }
    }
    
    // Methode zum Füllen des Arrays mit zufälligen Zahlen
    private static void fillArrayRand(int[] arr) {
        java.util.Random numberGenerator = new java.util.Random();
        assert arr.length > 0 : "Das Array hat die Länge 0";
        for (int i = 0; i < arr.length; i++) {
            
            arr[i] = numberGenerator.nextInt(arr.length);
        }
    }
    
     // Methode zum Füllen des Arrays mit mit aufsteigend Sortierten Zahlen
    private static void fillArrayAuf(int [] arr){
        assert arr.length > 0 : "Das Arrayhat die Länge 0";
        for ( int i = 0 ; i < arr.length ; i ++){
            arr[i] = i ;        
        }
    }
    
    // Methode zum Füllen des Arrays mit absteigend sortierten Zahlen
    private static void fillArrayAb(int[] arr) {

        assert arr.length > 0 : "Das Array hat die Länge 0";

        for (int i = arr.length -1; i > 0; i--) {
            arr[i] = i;
        }
    }

    public static void insertionSort(int[] Zahlen){
        assert Zahlen.length > 0: "Das Array ist leer";
        for( int j = 1 ; j < Zahlen.length ; j++){
            
            int key = Zahlen [j] ;
            int i = j-1 ;
            while ( i >= 0 && Zahlen [i] < key ){
                
                Zahlen[i+1] = Zahlen[i];
                i = i - 1;
            }
            Zahlen[i+1] = key;
        }
    }

    private static boolean isSorted(int[] a){

        assert a.length > 0: "Das Array a ist leer. Es kann nicht auf Sortierung geprüft werden!";

        for(int i = 0 ; i < a.length -1 ; i++){
            if(a[i]<a[i+1]){
                return false;
            }
        }
        return true;
    }

    private static void printArray(int[] arr) {
        assert arr.length > 0: "Das Array ist leer. Es kann nichts ausgegeben werden";
        
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private static String usageInfo() {
        return "\n\n Usage: java insertionSort n [filltype] \n"
            + "Der Parameter n ist immer erforderlich und muss eine natürliche Zahl > 1 darstellen.\n"
            + "Der Parameter filltype ist optional. Er gibt an wie das Array befüllt werden soll.\n"
            + "Mögliche Werte sind 'rand', 'auf', 'ab' \n";
    }
}