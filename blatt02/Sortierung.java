public class Sortierung {
    public static void main( String[] args){

        // Nach Aufgabenstellung sind nur 2 Parameter erlaubt
        if(args.length > 3){
            System.out.println("FEHLER: Es wurden zu viele Parameter übergeben." + usageInfo());
            System.exit(1);
        }

        // Nach Aufgabenstellung muss es mindestens einen Paramter geben
        if( args.length < 1){
            System.out.println("FEHLER: Es muss mindestens die Länge des Feldes als Parameter  übergeben werden." + usageInfo());
            System.exit(1);
        }

        // Der erste Paramter muss eine natürliche Zahl sein.
        int a = 0;
        try{
            a = Integer.parseInt(args[0]);
        }
        catch(NumberFormatException e ){
            System.out.println("FEHLER: Der erste Parameter ist keine natürliche Zahl " + usageInfo());
            System.exit(1);
        }

        // Der erste Parameter muss größer als 1 sein, da sonst nicht sotiert werden kann.
        if(a < 1){
            System.out.println("FEHLER : Der erste Parameter ist nicht größer 1" + usageInfo());
            System.exit(1);
        }

        int[] array = new int [a];

        // Gewünschte Sortierung des Arrays ermitteln
        if(args.length == 3) {

            switch (args[2]) {
                case "rand": // Füllen des Arrays mit zufälligen Zahlen
                    fillArrayRand(array);
                    break;
                case "auf": // Füllen des Arrays mit mit aufsteigend Sortierten Zahlen
                    fillArrayAuf(array);
                    break;

                case "ab":
                    fillArrayAb(array); // Füllen des Arrays mit absteigend sortierten Zahlen
                    break;

                default: 
                    System.out.println("FEHLER: Der zweite Parameter enthält eine unzulässige Zeichenkette!" + usageInfo());
                    System.exit(1);
                    break;
            }
        } else {
            fillArrayRand(array);  // Füllen des Arrays mit zufälligen Zahlen
        }
        
        // Nach Aufgabestellung soll das Array bei weniger als 100 Elemente ausgegeben werden.
        if (array.length < 100) printArray(array);
        
        
        // Variablen zur Zeitmessung
        long tStart, tEnd, msecs;
        tStart = System.currentTimeMillis();
        // Prüfung ob insertionsort oder mergesort
        if ( args.length >= 2){ 
            if( args[1].equals("insert")){
                insertionSort(array);
            }  
            else if (args[1].equals("merge")){
                mergeSort(array);
            }
            else {
                System.out.println("Die Eingabe: " + args[1] + " ist an dieser Stelle nicht zulässig. Zuläsige Argumente sind 'merge' oder 'insert'" + usageInfo() );
                System.exit(1);
            }
        }
        else{ 
            mergeSort(array);
            
        }
        
        
        
        
        tEnd = System.currentTimeMillis();
        msecs = tEnd - tStart;
        
        if(isSorted(array)){
            
            // Nach Aufgabestellung soll das Array bei weniger als 100 Elemente ausgegeben werden.
            if(array.length < 100) printArray(array);
        
            System.out.println("Feld ist sortiert!");
            System.out.println ("Das Sortieren des Arrays hat " + msecs + " ms gedauert.");
            /**
             * Testing Output
            System.out.println(msecs);
            */
        }
        else{
            System.out.println("FEHLER: Feld ist nicht sortiert!");
        }
    }
    // Methode zum Füllen des Arrays mit zufälligen Zahlen
    private static void fillArrayRand(int[] arr) {
        java.util.Random numberGenerator = new java.util.Random();
        assert arr.length > 0 : "Das Array hat die Länge 0";
        for (int i = 0; i < arr.length; i++) {
            
            arr[i] = numberGenerator.nextInt(arr.length);
        }
    }
    
     // Methode zum Füllen des Arrays mit mit aufsteigend Sortierten Zahlen
    private static void fillArrayAuf(int [] arr){
        assert arr.length > 0 : "Das Arrayhat die Länge 0";
        for ( int i = 0 ; i < arr.length ; i ++){
            arr[i] = i ;        
        }
    }
    
    // Methode zum Füllen des Arrays mit absteigend sortierten Zahlen
    private static void fillArrayAb(int[] arr) {

        assert arr.length > 0 : "Das Array hat die Länge 0";
        for (int i = arr.length -1; i > 0; i--) {
            arr[i] = i;
        }
    }

    private static void mergeSort(int[] arr) {
        int[] tmp = new int[arr.length];
        mergeSort(arr, tmp, 0, arr.length-1);
        
    }

    private static void mergeSort(int[] arr, int[] tmp, int start, int ende) {
        if(start < ende) {
            int m = (start + ende) /2;
            mergeSort(arr, tmp, start, m);
            mergeSort(arr, tmp, m+1, ende);
            merge(arr, tmp, start, m, ende);
        }
        
    }
    
    public static void merge(int[] arr, int[] tmp, int left, int middle, int right) {

        // Die beiden Arrays in das tmp Array übertragen
        for (int i = left; i <= right; i++) {
            tmp[i] = arr[i];
        }

        // Indizes setzten
        int i = left;
        int j = middle + 1;
        int k = left;

        // Den kleinsten wert aus er linke oder rechten seite in das Originale Array schreiben
        while (i <= middle && j <= right) {
            if (tmp[i] >= tmp[j]) {
                arr[k] = tmp[i];
                i++;
            } else {
                arr[k] = tmp[j];
                j++;
            }
            k++;
        }
        // Alle Übrigen werte werte der linken seite Übernehmen.
        while (i <= middle) {
            arr[k] = tmp[i];
            k++;
            i++;
        }
        
        // Da am Anfang beide Array kopiert wurden sind die werte der Rechten seite schon an
        // der richtigen Stelle

    }

    private static void insertionSort(int[] Zahlen){
        assert Zahlen.length > 0: "Das Array ist leer";
        for( int j = 1 ; j < Zahlen.length ; j++){
            
            int key = Zahlen [j] ;
            int i = j-1 ;
            
            while ( i >= 0 && Zahlen [i] < key ){
                
                Zahlen[i+1] = Zahlen[i];
                i = i - 1;
                
            }
            Zahlen[i+1] = key;
        }
    }
    private static boolean isSorted(int[] a){

        assert a.length > 0: "Das Array a ist leer. Ein leeres Array hat keine Sortierung!";

        for(int i = 0 ; i < a.length -1 ; i++){
            if(a[i]<a[i+1]){
                return false;
            }
        }
        return true;
    }

    private static void printArray(int[] arr) {
        assert arr.length > 0: "Das Array ist leer. Es kann nichts ausgegben werden!";
        
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private static String usageInfo() {
        return "\n\n Usage: java insertionSort n [filltype] \n"
            + "Der Parameter n ist immer erforderlich und muss eine natürliche Zahl > 1 darstellen.\n"
            + "Der zweite Parameter Sortierungs-Typ ist optional. Er gibt an wie das Array sortiert werden soll."
            + "Wenn kein Sortierungs-Typ angegeben wurde, wird mergeSort ausgeführt \n"
            + "Der Parameter filltype ist optional. Er gibt an wie das Array befüllt werden soll.\n"
            + "Mögliche Werte sind 'rand', 'auf', 'ab' \n";
    }
}
