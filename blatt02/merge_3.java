class merge_3{

    public static void main(String[] args) {
        int[] array = {10, 498, 4, 332, 31, 6, 7, 2, 3,78};
        printArray(array);

        mergeSort3(array, 0, array.length -1);
        printArray(array);
    }

    private static void merge3(int []  A , int l, int m1, int m2, int r) {    
        
        int[] B = new int[A.length];
        int tmpR = r;
        r = m2;
        int i;
        int m = m1;
        int i1 = l;
        int i2 = m+1;
        
        for(int j = 1 ; j <= r-l+1; j++){

            if (i2 > r || (i1 < m +1 && A[i1] <= A[i2])) {
                i = i1;
                i1 = i1 +1;
            }
            else{
                i = i2;
                i2 = i2 +1;
            }

            B[l - 1 + j] = A[i];
        }

        
        for(i = l; i <= r; i++) {
            A[i] = B[i];
        }

        B = new int[A.length];
        i = 0;
        m = m2;
        i1 = l;
        i2 = r+1;
        r = tmpR;
        
        for(int j = 1 ; j <= r-l+1; j++){

            if (i2 > r || (i1 < m +1 && A[i1] <= A[i2])) {
                i = i1;
                i1 = i1 +1;
            }
            else{
                i = i2;
                i2 = i2 +1;
            }

            B[l - 1 + j] = A[i];
        }

        
        for(i = l; i <= r; i++) {
            A[i] = B[i];
        }

    }

    private static void merge2(int[] A, int l, int m, int r){
        int[] B = new int[A.length];
        int i;
        int i1 = l;
        int i2 = m+1;
        
        for(int j = 1 ; j <= r-l+1; j++){

            if (i2 > r || (i1 < m +1 && A[i1] <= A[i2])) {
                i = i1;
                i1 = i1 +1;
            }
            else{
                i = i2;
                i2 = i2 +1;
            }

            B[l - 1 + j] = A[i];
        }

        
        for(i = l; i <= r; i++) {
            A[i] = B[i];
        }
    }
    public static void mergeSort3(int[] A , int l , int r){
        if(l<r){
            int k = (r-l)/3;
            int m1 = l + k;
            int m2 = l + 2*k;
            mergeSort3(A, l , m1);
            mergeSort3(A, m1+1 , m2);
            mergeSort3(A, m2+1, r);
            merge3(A,l,m1,m2,r);
        }
    }

    public static void mergeSort2(int[] A , int l , int r){
        if(l<r){
            int m1 = (r+l)/2;
            mergeSort2(A, l , m1);
            mergeSort2(A, m1+1, r);
            merge2(A, l, m1,r);
        }
    }


    private static void printArray(int [] a) {
        System.out.println();
        for (int i : a) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }
}