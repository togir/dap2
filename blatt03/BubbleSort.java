public class BubbleSort{

    public static void main(String[] args) {
        int [] zahlen = fillArray(50000);
        long startT, endT; 
        double diffT;

        startT = System.currentTimeMillis();
        BubbleSort(zahlen);
        endT = System.currentTimeMillis();

        diffT = endT - startT;

        System.out.println(diffT / 1000.0 + "s");

    }

    /**
     * Die Methode gibt ein absteigend gefülltes INT Array mit n Elementen zurück.
     */
    private static int[] fillArray(int n) {

        int[] array = new int[n];

        for (int i =0; i< array.length; i++) {
            array[i] = n--;
        }        
        return array;
    }

    /**
    * BubbleSort vergleicht zwei Array Indizes und vertauscht diese falls die  Zahl im 
    * kleineren Index größer ist als die Zahl im größeren Index.
    * Dies wird für das ganze Teilarray ausgeührt(innere Schleife), sodass am Ende an Index i die kleinste Zahl steht.
    * Beim Wiederholen dieses Vorgangs wird eine linke Grenze um jeweils 1 erhöht, bis nur 1 Elemente übrig ist(äusere Schleife)
    */ 
    public static void BubbleSort(int [] a){
        int n = a.length ;

        for (int i = 1; i < n ; i++){

            for(int j = n ; j < i+1 ; j--){
                
                if(a[j-1]>a[j]){
                        int tmp = a[j];
                        a[j] = a[j-1];
                        a[j-1] = tmp;
                }
            }
            
        }
    }
}