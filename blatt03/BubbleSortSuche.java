public class BubbleSortSuche {
    public static void main(String[] args) {

        // Variable für den zu erreichenden wert.
        float a = 0;

        if(args.length < 1) {
            System.out.println("FEHLER: Es muss eine Zahl als erster Paramter für die Dauer der Suche mitgegeben werden.\n" + usageInfo());
            System.exit(1);
        }

        if(args.length > 1) {
            System.out.println("FEHLER: Es ist nur ein Parameter erlaubt!\n" + usageInfo());
            System.exit(1);
        }
        
        try{
            a = Float.parseFloat(args[0]);
        }
        catch(NumberFormatException e){
            System.out.println("FEHLER: Der eingegebene Parameter ist keine Zahl." + usageInfo());
        }

        if(a <= 0) {
            System.out.println("FEHLER: Die Zeitangabe ist negativ. " + usageInfo());
        }
        
        // Ein erster durchlauf mit 1000 Elementen
        int n = 1000;
        double time = 0.0;
        while(time <= a) {
            
            time = bubbleSort(fillArray(n));
            System.out.println("Size: " + n + ", Time: " + time );
            n = n*2;
        }

        tertiaereSuche(n/2, n, a);
    }

    /**
     * Die Methode gibt ein absteigend gefülltes INT Array mit n Elementen zurück.
     */
    private static int[] fillArray(int n) {

        int[] array = new int[n];

        for (int i =0; i< array.length; i++) {
            array[i] = n--;
        }        
        return array;
    }

    /**
    * BubbleSort vergleicht zwei Array Indizes und vertauscht diese falls die  Zahl im 
    * kleineren Index größer ist als die Zahl im größeren Index.
    * Dies wird für das ganze Teilarray ausgeührt(innere Schleife), sodass am Ende an Index i die kleinste Zahl steht.
    * Beim Wiederholen dieses Vorgangs wird eine linke Grenze um jeweils 1 erhöht, bis nur 1 Elemente übrig ist(äusere Schleife)
    */ 
    public static double bubbleSort(int [] a){

        long startT, endT; 

        startT = System.currentTimeMillis();
        

        int n = a.length ;
        for (int i = 1; i < n ; i++){
            for(int j = n ; j < i+1 ; j--){
                if(a[j-1]>a[j]){
                        int tmp = a[j];
                        a[j] = a[j-1];
                        a[j-1] = tmp;
                }
            }
            
        }

        endT = System.currentTimeMillis();
        
        return (double) ((double) endT - startT) / 1000;

    }
    /**
     * Zwischen den letzten beiden Feldgrößen (eine mit kürzerer und eine mit längerer Laufzeit) wird eine tertiäre Suche gestartet.
     * Sie teilt den Suchbereich in drei gleichgroße Teile auf. 
     */
    private static void tertiaereSuche(int start, int ende, float ziel){
        int step = start / 3;

        // Zeitwerte für die verschiedenen Feldgrößen ermitteln
        double wertStart = bubbleSort(fillArray(start));
        double intervall1 = bubbleSort(fillArray(start + step));
        double intervall2 = bubbleSort(fillArray(start + step*2));
        double wertEnde = bubbleSort(fillArray(ende));
        // Prüfung ob eine der Zeiten um 0,1s von der gesuchen Zeit abweicht. Wenn ja wird das Ergebniss ausgegeben.
        // return; bricht die ausführung ab da eine passende größe des Arrays ermittelt wurde.
        if(Math.abs(wertStart - ziel) < 0.1){
            printResult(start, wertStart);
            return;
        }
        
        if(Math.abs(intervall1 - ziel) < 0.1 ){ 
            printResult(start+step, intervall1);
            return;
        }
        
        if(Math.abs(intervall2 - ziel) < 0.1) {
            printResult(start+step*2, intervall2); 
            return;
        }
        
        if(Math.abs(wertEnde - ziel) < 0.1) {
            printResult(ende, wertEnde);
            return;
        }
        //Falls keine der Zeiten eine gute Näherung ist , werden die Zwischenergebnisse ausgegeben und
        // ein erneuter Aufruf von tertiaererSuche , mit verkleinerten Suchbereich ,wird gestartet.
        System.out.println("Sizes: " + start + ", " + (start + step) + "," + (start + step*2) + ", " + ende );
        System.out.println("Sizes: "  + wertStart + ", " + intervall1 + ", " + intervall2 + "," + wertEnde);

        
        if ( intervall1 < ziel){
                tertiaereSuche(start, start + step, ziel);

        } else  if(intervall2 < ziel) {
            tertiaereSuche( start + step, start+ step*2, ziel);

        } else {
            tertiaereSuche( start+ step*2, ende, ziel);
        }
    }

    private static void printResult(int size, double time) {
        System.out.println("Size: " + size + ", Time: " + time);
    }

    private static String usageInfo() {
    return  "\n \n USAGE: \n"
        +   "java BubleSortSuche n \n"
        +   "n muss eine Kommazahl sein. Das Dezimaltrennzeichen ist ein '.'\n";
    }
}