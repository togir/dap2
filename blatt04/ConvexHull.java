import java.util.ArrayList;

public class ConvexHull {
    
    public static ArrayList<Point> computeHull(ArrayList<Point> inputList) {
        // Abbruch der Rekursion wenn Liste 3 Elemente hat
        if(inputList.size() <= 2) return inputList;

        // Sortieren nach der ersten Koordianten (x)
        inputList.sort(Point.FirstCoordiantesComparator);

        return rekComputeHull(inputList);
    }

    /**
     * FIXME
     * @param inputList
     * @return ArrayList
     */
    private static ArrayList<Point> rekComputeHull(ArrayList<Point> inputList){
    
        if(inputList.size() > 3) {

            //Berechnen der Hüllen mit Teillisten links und rechts
            ArrayList<Point> left = rekComputeHull( new ArrayList<Point>(
                                        inputList.subList(0, inputList.size() / 2)
                                        )
                                    );
            ArrayList<Point> right = rekComputeHull( new ArrayList<Point>(
                                        inputList.subList(inputList.size()/2, inputList.size())
                                        )
                                    );


            int l = left.size()-1; // links           
            int r = 0;  // rechts
            
            // System.err.println("neuer liste");
            // System.out.println("left.size " + left.size() );
            // Obere Linie finden
            while(true) {
                int prev_l = l;
                int prev_r = r;
                // Wenn rechts noch ein element ist
                while( right.size() -1 > r && Line.side(left.get(l), right.get(r), right.get(r+1)) == 1) {
                    r++;
                }
                
                while( l > 0 && Line.side(left.get(l), left.get(l -1), right.get(r)) == 1) {
                    l--;
                    // System.out.println(l);
                }

                if ( prev_l == l && prev_r == r )
                    break;
            }

                int left_top = l;
                int right_top = r;

                // Untere verbindung
                l = 0; // links           
                r = right.size() -1;  // rechts

                // Hier gibt es noch probleme die linke Hälfte im Uhrzeigersinn
                // und die rechte gegen den Uhrzeigersinn laufen zu lassen. Demach
                // ist auch das endergebiss falsch!
                while(true) {
                    int prev_l = l;
                    int prev_r = r;
                    // Wenn rechts noch ein element höher
                    while(r > 0 && Line.side(left.get(l), right.get(r), right.get(r-1)) == -1) {
                        r--;
                    }
                

                    // Wenn es Links ein element weiter unten gibt
                    while(left.size() -1 > l && Line.side(left.get(l), left.get(l +1), right.get(r)) == -1) {
                        l++;
                    }
                

                    if ( prev_l == l && prev_r == r )
                        break;
                }

                ArrayList<Point> hull = new ArrayList<Point>();

                hull.addAll(left.subList(0, left_top)); // Linke Liste bis zur Oberen Linie
                hull.addAll(right.subList(right_top, right.size()-1)); // Ankunft rechte obere Linie bis zum ende de Liste
                hull.addAll(right.subList(0, r)); // vom anfang bis zu dem Punkt an dem die Linie nach Linls zurückgeht
                hull.addAll(left.subList(l, left.size()-1)); // Ankunft Linke untere Linie bis ende.

                return hull;
        // Ende Funktion -----------------------------      
        }
        // Alle doppelten entfernen
        for (int i = 1; i < inputList.size(); i++) {
            if(inputList.get(i-1).equals(inputList.get(i))) {
                inputList.remove(i-1);
            }
        }

        assert inputList.size() > 0: "Es wurde alle Elemente gelöscht!";

        if(inputList.size() >3) { // Hier muss kleiner hin!!!!!!!
            return inputList;
        }

        if(Line.side(inputList.get(0), inputList.get(1), inputList.get(2)) == 0) {
            inputList.remove(1);
        }
        assert inputList.size() > 0: "Es wurde alle Elemente gelöscht!";
        
        /**
         * Der Merge Merge-Algorithmus setzt voraus das dass 2. Listenelement einer
         * Dreierliste über der ersten liegt oder beide niedriger liegt.
         * */
        if(Line.side(inputList.get(0), inputList.get(1), inputList.get(2)) == 1) {
            Point tmp = inputList.get(1);
            inputList.set(1, inputList.get(2));
            inputList.set(2, tmp);
        }
        
        assert inputList.size() > 0: "Es wurde alle Elemente gelöscht!";
        return inputList;
    }
}