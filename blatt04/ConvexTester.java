import java.util.ArrayList;

public class ConvexTester {
    public static void main(String[] args) {

        assert testLeftRight(): "Seitenfehler";
        assert testFirstCoordinatesSort(): "Sortierungsfehler" ;
        assert testConvexHull(): "Hüllenfehler";

        double a = 0 , b = 0 ;
        int c = 0;
        if(args.length != 3){
            System.out.println("FEHLER: Es wurde eine ungültige Anzahl an Parameter mitgegeben. " + usageInfo());
            System.exit(1);
        }
        try{
            a =  Double.parseDouble(args[0]);
        }
        catch(NumberFormatException e){
            System.out.println("FEHLER: Der eingegebene Parameter a entspricht nicht dem Datentyp double." + usageInfo());
            System.exit(1);
        }
        try{            
            b = Double.parseDouble(args[1]);        
        }
        catch(NumberFormatException e){
            System.out.println("FEHLER: Der eingegebene Parameter b entspricht nicht dem Datentyp double." + usageInfo());
            System.exit(1);
        }

        if(b <= a) {
            System.out.println("FEHLER: Die Obergrenze muss größer als die Untergrenze sein!"+ usageInfo());
            System.exit(1);
        }

        try{
            c = Integer.parseInt(args[2]);
        }
        catch(NumberFormatException e){
            System.out.println("FEHLER: Der eingegebene Parameter c entspricht nicht dem Datentyp int"+ usageInfo());
            System.exit(1);
        }
        if(c < 1) {
            System.out.println("Fehler: Die Anzahl der Elemente in der List muss mindestens 1 sein!" + usageInfo());
            System.exit(1);
        }

        PointsGenerator pg = new PointsGenerator(a, b);
        ArrayList<Point> liste = pg.genrate(c);

        // Anzahl der generierten Punkte
        System.out.println(liste.size());
        printList(liste);
        
        ArrayList<Point> hull = ConvexHull.computeHull(liste);
        
        // Anzahl der Eckpunkte
        System.out.println(hull.size());
        printList(hull);

        int notInHull = countAllPointsNotInHull(liste, hull);
        if( notInHull == 0) {
            System.out.println("Alle Punkte sind in der Huelle");
        } else {
            System.out.println(notInHull + " Punkte liegen nicht in der Huelle");
        }


    }

    public static String usageInfo() {
        return  "\n\n java ConvexTester a b c \n"
            +   "Die Anzahl an Parameter die mitgegeben werden muss ist 3. \n"
            +   "Der Parameter a ist die untere Schranke für die Koordinaten und wird mit dem Datentyp double angegeben. \n"
            +   "Der Parameter b ist die obere Schranke für die Koordinaten und wird mit dem Datentyp double angegeben. \n"
            +   "Der Parameter c ist die Anzahl zu generierender Punkte und akzeptiert werden nur positive int Werte.";
    }

    private static boolean testLeftRight() {
        
        Point[] a =  {
            new Point(1, 1),
            new Point(2,2),
            new Point(1, 1),
            new Point(4, 4),
            new Point(5 ,5),
            new Point(-3, 5),
            new Point(3,3),
            new Point(3,3),
            new Point(3.5, 5),
        };
        Point[] b = { 
            new Point(2,2),
            new Point(1, 1),
            new Point(4, 4),
            new Point(1, 1),
            new Point(3 , -5),
            new Point(5 ,5),
            new Point(3, -10),
            new Point(3, 10),
            new Point(6, 5.5),
        };
        Point[] c = {
            new Point(0, 1),
            new Point(0, 1),
            new Point(3, 4),
            new Point(3, 4),
            new Point(-2, 10),
            new Point(-2, 10),
            new Point(3, -100),
            new Point(3, 100),
            new Point(4.5, 5)
        };

        int[] result = {-1, 1, -1, 1, 1, -1, 0, 0, 1};

        for(int i = 0; i < a.length; i++) {
            Line line = new Line(a[i], b[i]);
            if (line.side(c[i]) != result[i]) {

                System.err.println("Fehler: Seite falsch berechnet an Index " + i);
                return false;
            }
        }

        return true;
    }
    
    private static boolean testFirstCoordinatesSort() {

        PointsGenerator generator = new PointsGenerator(-100, 100);
        ArrayList<Point> pointList = generator.genrate(20);

        pointList.sort(Point.FirstCoordiantesComparator);
        
        for (int i = 1; i < pointList.size(); i++) {
            if (pointList.get(i-1).coordinatesList.get(0) > pointList.get(i).coordinatesList.get(0)) {
                System.err.println("Sortierung ist falsch an Index " + i);
                return false;
            }
        }
        return true;
    }

    private static boolean testConvexHull() {
        ArrayList<Point> all = new ArrayList<Point>();
        all.add(new Point(1,1));
        all.add(new Point(1.5,4));
        all.add(new Point(4,2));
        all.add(new Point(3.5,5));
        all.add(new Point(4.5,4));
        all.add(new Point(6.5,2));
        all.add(new Point(6, 5.5));
        all.add(new Point(4.5, 5));
        all.add(new Point(7,6.5));
        all.add(new Point(7.5,3.5));
        all.add(new Point(8.5,1));
        all.add(new Point(8.5,4.5));


        ArrayList<Point> convexHull = ConvexHull.computeHull(all);

        for (Point point : all) {
            if(!convexHull.contains(point)) {
                System.out.println("Entfert: " + point + "\n");
            }
        }

        printList(convexHull);


        for (int i = 1; i < convexHull.size(); i++) {

            for (Point point : all) {
                if(Line.side(convexHull.get(i-1), convexHull.get(i), point) < 0) {
                    System.out.println("Linie: " + convexHull.get(i-1) + " nach " + convexHull.get(i));
                    System.out.println("Fehler: " + point + " ist nicht in der Hülle!");
                    return false;
                }
            }
        }

        // Zufallsliste
        PointsGenerator pg = new PointsGenerator(0, 20);
        ArrayList<Point> all2 = pg.genrate(100);

        ArrayList<Point> convexHull2 = ConvexHull.computeHull(all2);
        
        System.out.println("Hülle: ");
        printList(convexHull2);

        for (int i = 1; i < convexHull2.size()-1; i++) {

            for (Point point : all2) {
                if(Line.side(convexHull2.get(i-1), convexHull2.get(i), point) < 0) {
                    System.out.println("Linie: " + convexHull2.get(i-1) + " nach " + convexHull2.get(i));
                    System.out.println("Fehler: " + point + " ist nicht in der Hülle!");
                    return false;
                }
            }
        }


        return true;
    }

    public static void printList(ArrayList<Point> inputList) {
        for (Point point : inputList) {
            System.out.println(point);
        }
    }

    private static int countAllPointsNotInHull(ArrayList<Point> inputList, ArrayList<Point> hull) {
        
        int c = 0;
        for (int i = 1; i < hull.size()-1; i++) {

            for (Point point : inputList) {
                if(Line.side(hull.get(i-1), hull.get(i), point) < 0) {
                    c++;
                }
            }
        }
        return c;
    }
}