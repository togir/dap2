public class Line {

    private double startx, starty, endex, endey;
    
    public Line (Point a , Point b){
        this.startx = a.get(0);
        this.starty = a.get(1);
        this.endex = b.get(0);
        this.endey = b.get(1);
    }
    
    /**
     * Gibt an auf welcher Seite der Line ein Punkt liegt
     * Blickrichtung ist von Start der Line zum Ende.
     * Gibt 0 wenn auf der Line
     * Gibt 1 wenn links der Line 
     * Gibt -1 wenn rechts der Line
     */ 
    public int side(Point c){
        return side(new Point(startx, starty), new Point(endex, endey), c);
    }

    /**
     * Gibt an ob ein Punkt (C) links, rechts, oder auf einem Strahl des Vekotors AB liegt.
     * 0 wenn auf dem Strahl <br>
     * -1 wenn rechts   <br>
     * 1 wenn links
     */
    public static int side(Point A, Point B, Point C) {
        double side = (B.get(0) - A.get(0))*(C.get(1) - A.get(1)) - (C.get(0) - A.get(0))*(B.get(1)-A.get(1));

        if(side < 0) {
            return -1;
        }
        if (side > 0) {
            return 1;
        }
        return 0;
    }
}
