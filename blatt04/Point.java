import java.util.ArrayList;
import java.util.Comparator;

public class Point {

    ArrayList<Double> coordinatesList = new ArrayList<Double>();
    
    public Point(double ... values) {
        
        for (double d : values) {
            coordinatesList.add(d);
        }
    }

    @Override
    public String toString() {
        String s = "";
        for (Double el : coordinatesList) {
            s = s + " " + el;
        }

        return s;
    }
    
    public double get(int i) {
        try {
            return coordinatesList.get(i);
        } catch (Exception e) {
            throw new IndexOutOfBoundsException("FEHLER: Der Index " + i + " ist nicht vorhanden");
        }
    }

    public boolean equals(Point a){
        
        if(a.coordinatesList.size() != this.coordinatesList.size()) {
            return false;
        }

        for (int i = 0; i < a.coordinatesList.size(); i++) {
            if(a.coordinatesList.get(i) != this.coordinatesList.get(i) ){
                return false;
            }
        }

        return true;
    }


    public static Comparator<Point> 
    FirstCoordiantesComparator = new Comparator<Point>() {
        public int compare(Point p1, Point p2) {
            if (p1.coordinatesList.get(0) > p2.coordinatesList.get(0) ) {
                return 1;
            }
            if(p1.coordinatesList.get(0) < p2.coordinatesList.get(0)) {
                return -1;
            }
            else{
                return 0;
            }
        }
    };


    public static Comparator<Point> 
    AlphabeticComparator = new Comparator<Point>() {
        public int compare(Point p1, Point p2) {
            if (p1.coordinatesList.get(0) > p2.coordinatesList.get(0) ) {
                return 1;
            }
            if(p1.coordinatesList.get(0) < p2.coordinatesList.get(0)) {
                return -1;
            }
            
            if(p1.coordinatesList.get(1) < p2.coordinatesList.get(1)) {
                return 1;
            }

            if(p1.coordinatesList.get(1) > p2.coordinatesList.get(1)) {
                return -1;
            }

            return 0;
        }
    };
}