import java.util.ArrayList;
public class PointsGenerator {

    double min, max;

    public PointsGenerator(double min, double max) {
        this.min = min;
        this.max = max;
    }

    public ArrayList<Point> genrate (int  n) {
        ArrayList<Point> pointList = new ArrayList<Point>();
        for (int i = 0; i < n; i++) {
            pointList.add(getRandTwoDimensionPoint(this.min, this.max));
        }
        return pointList;
    }

    private Point getRandTwoDimensionPoint(double min, double max) {
        java.util.Random generator = new java.util.Random();
        double randomNumber1 = min + (max - min) * generator.nextDouble();
        double randomNumber2 = min + (max - min) * generator.nextDouble();

        return new Point(randomNumber1, randomNumber2);
    }
}