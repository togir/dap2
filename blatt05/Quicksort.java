
public class Quicksort {

    public static void main(String[] args) {
        //Parameter Überprüfung
        if( args.length > 1){
            System.out.println("FEHLER: Es wurden zu viele Parameter mitgegeben");
            System.exit(1);
        }

        if( args.length < 1){
            System.out.println("FEHLER: Es wurden zu wenige Parameter mitgegeben");
            System.exit(1);
        }

        int a = 0 ;
        try {
            a = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println("FEHLER: Der mitgegebene Parameter ist kein Integer");
        }

        if (a < 1 ){
            System.out.println("FEHLER : Der erste Parameter ist nicht größer 1");
            System.exit(1);
        }
        
        int [] array = fillArray(a);


        // Variablen zur Zeitmessung
        long tStart, tEnd, msecs;
        tStart = System.currentTimeMillis();
        quicksort(array,0, array.length -1);
        tEnd = System.currentTimeMillis();
        msecs = tEnd - tStart;

        assert isSorted(array) : "FEHLER: Quicksort hat nicht sottiert!";
        if(isSorted(array)){                        
            System.out.println("Feld ist sortiert!");
            System.out.println ("Das Sortieren des Arrays hat " + msecs + " ms gedauert.");
            //for(int i = 0 ; i < array.length ; i ++){
            //    System.out.print(array[i] + " , ");
            //}
            System.out.println();

        }
        else{
            System.out.println("FEHLER: Feld ist nicht sortiert!");
        }
    }
    
    //Bestimmung des mittleren Wertes von 3 Zahlen
    private static int med3(int[] a, int l, int r) {
        return median(a[l], a[(l + r) / 2], a[r]);
    }
    
    /**
    * Gibt einen approximierten Mittelwert zurück
    * Dabei wird das Array in 3 Intervalle geteilt
    * [l..1/3] , [1/3...2/3] , [2/3...r]
    * In diesen Intervallen wird jeweils der Median bestimmt
    * Von diesen 3 Medianen wird der Median für das gesamte Array gebildet
    */
    private static int ninther(int[] a, int l, int r) {
        int m1 = l + (r - l) * 1/3;
        int m2 = l + (r - l) * 2/3;
        return median(med3(a, l, m1), med3(a, m1, m2), med3(a, m2, r));
    }

    private static int median(int a, int b, int c) {
        int median = a;

        if ((a <= b && b <= c) || (c <= b && b <= a)) {
            median = b;
        }
        
        if ((a <= c && c <= b) || (b <= c && c <= a)) {
            median = c;
        }

        return median;
    }
    /**
     * Quicksort sortiert jeweils pro Durchlauf nach 1 Element.
     * Dieses Element ist der Median der durch ninther() ermittelt wird
     * Dann werden die Elemente geprüft ob sie größer oder kleiner als der Median sind
     * Größere werden links vom Median geschrieben und kleinere Links
     * Dieser Vorgang wird wiederholt bis nur 1 Element übrig ist
     * Bei ca. 74000 Elemente schaft es der Quicksort nicht mehr zuverlässig die Elemente zu sortieren
     * Ab 10000 Elemente wird das Array so gut wie nie Sortiert
     */
    public static void quicksort( int [] A , int l , int r) {

        if ( l < r){
            int i = l ;
            int j = r ;
            int pivot = ninther(A , l , r);
            while (i <= j ){
                while (A[i] < pivot) {
                    i ++;
                }
                while ( A[j] > pivot){
                    j--;
                }
                if(i <= j){
                    int tmp = A[i];
                    A[i] = A[j];
                    A[j] = tmp ;
                    i ++;
                    j--;
                }
            }
            quicksort(A,l,j);
            quicksort(A, i , r);
        }
    }
    
    private static int[] fillArray(int n) {
        assert n > 0: "FEHLER: Ein Array mit 0 Elementen kann nicht gefüllt werden";
        java.util.Random generator = new java.util.Random();

        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = generator.nextInt();
        }

        assert array.length == n : "Das Array ist nicht so lang wie verlangt!!";
        return array;
    }

    private static boolean isSorted(int[] a){

        assert a.length > 0: "FEHLER: Das Array a ist leer. Es kann nicht auf Sortierung geprüft werden!";

        for(int i = 0 ; i < a.length -1 ; i++){
            if(a[i] >= a[i+1]){
                return false;
            }
        }
        return true;
    }


}