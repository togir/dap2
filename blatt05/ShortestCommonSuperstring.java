import java.util.Random;
import java.util.ArrayList;

public class ShortestCommonSuperstring {
    
    public static void main(String[] args) {
        int a = 0;
        ArrayList<String> stringList = new ArrayList<String>();

        if(args.length < 1) {
            System.err.println("FEHLER: Es muss mindestens ein Argument angegeben werden.");
            System.exit(1);
        }

        if( args.length == 1) {
            try{
                a = Integer.parseInt(args[0]);
                if(a <= 1) {
                    System.err.println("FEHLER: Die Zahl muss größer als 1 sein.");
                    System.exit(1);
                }
            } catch(NumberFormatException e) {
                // do nothing 
            }
        }

        // Strings von der Kommandozeile "einlesen" fals keine Zahl angegeben wurde
        if(a == 0) {
            // Setzt die Arraylist auf eine Minimale länge.
            stringList.ensureCapacity(args.length);
            for (String string : args) {

                if(string.length() < 2) {
                    System.err.println("FEHLER: Die Zeichenketten müssen alle mindesten die Länge 2 haben.");
                    System.exit(1);
                }

                stringList.add(string.toUpperCase());
            }
        } else {
            stringList.ensureCapacity(a);
            for (int i = 0; i < a; i++) {
                stringList.add(generateRandomString());
            }
        }

        // Ausführliche Ausgabe falls die Eingabemenge kleiner als 11 ist
        boolean output = false;
        if(stringList.size()<= 10){
            output = true;
            for (int i = 0; i < stringList.size(); i++) {
                System.out.print(stringList.get(i) + " ");
            }
            System.out.println();
        }

        while(stringList.size() > 1) {
            /**
             * Invariante:
             * Die übergebene Liste wird nach dem Aufruf
             * von combineMaxOverlapp(liste) um ein Element kleiner.
             */ 
            combineMaxOverlapp(stringList, output);
            if(output) {
                for (int i = 0; i < stringList.size(); i++) {
                    System.out.print(stringList.get(i) + " ");
                }
                System.out.println();
            }
        }

        System.out.println("Superstring " + stringList.get(0) + " mit Länge " + stringList.get(0).length() + " gefunden.");
    }

    /**
     * Kombiniert 2 Strings mit einer größten Überlappung zu einem Superstring
     * @param list
     */
    private static void combineMaxOverlapp(ArrayList<String> list, boolean output) {
        if(list.size() <= 1) {
            assert false: "FEHLER: Die Liste besteht nur aus einem String. Deshalb ist es unnötig einen Superstring zu bilden.";
            return;
        }
        int max = 0;
        int a = 0;
        int b = 1;

        for (int i = 0; i < list.size(); i++) {
            
            for (int j = 0; j < list.size(); j++) {

                if(i == j) {
                    continue;
                }

                int overlap = stringOverlap(list.get(i), list.get(j));
                if(max < overlap) {
                    max = overlap;
                    a = i;
                    b = j;
                }

                overlap = stringOverlap(list.get(j), list.get(i));
                if(max < overlap) {
                    max = overlap;
                    a = j;
                    b = i;
                }
            }
        }

        // Es ist wichtig das erst ersetzt wird und dann gelöscht, wegen der Indexverschiebung beim Löschen.
        list.set(a, combineTwoStrings(list.get(a), list.get(b), max, output));
        list.remove(b);
    }

    /**
     * Kombiniert 2 Strings wobei am ersten String die Überlappenden Elemente entfert werden.
     * o =  Anzahl der überlappenden Buchstaben
     * output = wenn TRUE dann wird ausgegeben wie der String ersetzt wurde
     */
    private static String combineTwoStrings (String a, String b, int overlap, boolean output) {
        assert a.length() < overlap: "FEHLER: Der String a kann nicht kleiner als die Überlappung sein.";
        if(a.length() == overlap) {
            if(output) System.out.println("Ersetze " + a + " und " + b + " durch " + b );
            return b;
        }

        String c = a.substring(0, a.length() - overlap) + b;
        if(output) System.out.println("Ersetze " + a + " und " + b + " durch " + c );
        
        return c;
    }

    private static int stringOverlap(String str1, String str2) {

        int m = str1.length();
        int n = str2.length();
        int max = 0;

        for (int i = 0; i < Math.min(m, n); i++) {
            if(str1.substring(m-i).equals(str2.substring(0, i))) {
                max = i;
            }
        }
        return max;
    }



    public static String generateRandomString() {
        String alphabet = "ABCD";
        Random numberGenerator = new java.util.Random();
        StringBuilder builder = new StringBuilder();
        int length = 3 + numberGenerator.nextInt(4);
        while (length-- > 0) {
        int randomIdx = numberGenerator.nextInt(alphabet.length());
        builder.append(alphabet.charAt(randomIdx));
        }
        return builder.toString();
    }

}