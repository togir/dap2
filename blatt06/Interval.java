import java.util.Comparator;
class Interval {

    private int start, end;

    public Interval (int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
    
    @Override
    public String toString() {
        return "[" + start + "," + end + "]";
    }
    //Prüfung auf Gleichheit für Intervalle
    public boolean equals(Interval a){
        if ( a.getStart() == this.start && a.getEnd() == this.end) {
            return true;
        }
        return false;
    }

    public static Comparator<Interval> 
    EndValueComparator = new Comparator<Interval>() {
        public int compare(Interval p1, Interval p2) {
            if (p1.getEnd() > p2.getEnd()) {
                return 1;
            }
            if(p1.getEnd() < p2.getEnd()) {
                return -1;
            }
            else{
                return 0;
            }
        }
    };


}