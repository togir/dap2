import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
/**
 * IntevalScheduling
 */
public class IntervalScheduling {

    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("FEHLER: Es darf nur ein Parameter angegen werden!");
            System.exit(1);
        }
        
        String path = args[0];
        ArrayList<Interval> intervals = new ArrayList<Interval>();
        try {
            BufferedReader file = new BufferedReader( new FileReader( path ) );
            
            String zeile = null;
            while((zeile = file.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(zeile,",");
                int start = Integer.parseInt(st.nextToken());
                int ende = Integer.parseInt(st.nextToken());
                Interval ivall = new Interval(start, ende);
                intervals.add(ivall);
            }

            file.close();

        } catch (IOException e){
            System.out.println("FEHLER: Die Datei kann nicht gelesen werden!");
            System.exit(1);
        } catch(IllegalArgumentException e ) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden!");
            System.exit(1);
        }

        // Array sortieren nach Endzeitpunkt
        ArrayList<Interval> sorted = new ArrayList<Interval>();
        sorted.addAll(intervals);
        sorted.sort(Interval.EndValueComparator);
        ArrayList<Interval> scheduled = intervalScheduling(sorted);

        // Intervalle ausgeben wenn Liste unter 50 Elemente beinhaltet
        if(intervals.size() <= 50 ){
            for (int i = 0; i < intervals.size(); i++) {
                System.out.print(intervals.get(i));
            }
            System.out.println();

            for (int i = 0; i < sorted.size(); i++) {
                System.out.print(sorted.get(i));
            }
            System.out.println();

            for (Interval interval : scheduled) {
                System.out.print(interval);
            }
            System.out.println();
        }
        System.out.println("Leerlaufzeit : " + idleTime(scheduled));
    }
    
    //Funktion nimmt sich das erste Element und eliminiert die überschneidenden Intervalle
    //und nimmt dann das nächste Intervall das den kürzesten Anfangswert hat 
    public static ArrayList<Interval> intervalScheduling(ArrayList<Interval> intervals){
        int n = intervals.size();
        ArrayList<Interval> A = new ArrayList<Interval>();
        A.add(intervals.get(0));
        int j = 0 ;
        
        for (int i = 1; i < n; i++) {
            if (intervals.get(i).getStart() >= intervals.get(j).getEnd()){
                
                if(!A.contains(intervals.get(i))) {
                    A.add(intervals.get(i));
                }
                j = i;
            }

        }
        return A;
    }
    
    //Berechnung der Leerlaufzeit 
    public static int idleTime(ArrayList<Interval> intervals){
        int l = 0;
        
        // Wenn der Startwert - des Endwertes Postitiv ist gibt es Leerlaufzeit.
        for (int i = 1; i < intervals.size(); i++) {
            if(intervals.get(i).getStart() - intervals.get(i -1).getEnd() > 0) {
                l = l + intervals.get(i).getStart() - intervals.get(i -1).getEnd();
            }
        }
        return l;
    }


}
