import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;
import java.io.IOException;

public class MaximaleTeilsumme {
    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("FEHLER: Es darf nur ein Parameter angegen werden!");
            System.exit(1);
        }
        
        String path = args[0];
        ArrayList<ArrayList<Integer>> values = parseCsv(path);
        for (ArrayList<Integer> zeile : values) {
            
            // Die ArrayList<Integer> wird in ein int[] umgewandelt da
            // die maxTeilsumme nach Aufgabenstellung ein int[] erwartet.
            int[] zeileArr = zeile.stream()
                                        .mapToInt(Integer::intValue)
                                        .toArray();
            
            System.out.println(maxTeilsumme(zeileArr));
        }
        
    }

    private static ArrayList<ArrayList<Integer>> parseCsv(String path) {
        try {
            BufferedReader file = new BufferedReader( new FileReader( path ) );
            
            ArrayList<ArrayList<Integer>> fileList = new ArrayList<ArrayList<Integer>>();
            String zeile = null;

            while((zeile = file.readLine()) != null) {

                StringTokenizer st = new StringTokenizer(zeile,",");

                ArrayList<Integer> zeileListe = new ArrayList<Integer>();
                while (st.hasMoreTokens()) {
                    zeileListe.add(Integer.parseInt( st.nextToken()));
                }
                fileList.add(zeileListe);
            
            }

            file.close();

            return fileList;
        } catch (IOException e){
            System.out.println("FEHLER: Die Datei kann nicht gelesen werden!");
            System.exit(1);
        } catch(IllegalArgumentException e ) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden!");
            System.exit(1);
        }

        return new ArrayList<ArrayList<Integer>>(); // Für den Compiler!!!
    }

    /**
     * Die Funktion berechent die größte Teilsumme in einem Array
     * mit einer Laufzeit O(n²).
     */
    private static Teilsumme maxTeilsumme( int [] input){
        int[] teilsumme = input;
        /**
         * Jeweils die Teilsummen von [0..i] berechenen und an stelle i speichern
         */
        for ( int  i = 1 ; i < input.length ; i ++ ){
            teilsumme[i]  = teilsumme[i-1]+teilsumme[i];
        }
        int sum = 0 ;
        int max = 0 ;
        int start = 0;
        int ende = 0;
        /**
         * Einmal alle möglichen Teilsummen bilden und mit dem Maximum vergleichen
         * 1. Durchlauf : Teilsummen [0..index]
         * 2. Durchlauf : Teilsummen [1...index]
         * ...         * 
         */
        for (int l = 0; l < input.length; l++) {
            // In jedem Durchlauf wird das Intervall [l...input.length] kleiner in dem die Teilsummenberechnet werden.
            // Pro Durchlauf wird das Maximum gespeichert und mit den neuen Teilsummen verglichen.
            for (int r = l; r < input.length; r++) {
                sum =teilsumme[r];
                // Die Summe von [0..l-1] abziehen damit nur die Teilsumme
                // von [l..r] berechent wird.
                if(l > 0 ){
                    sum -= teilsumme [l-1];
                }
                // Wenn die Teilsumme [l..r] größer ist als eine vorherige Teilsumme
                // wird sie das neue Maximum
                if(sum > max ){
                    max = sum;
                    start = l;
                    ende = r;
                }
            }
        }
        return new Teilsumme ( start , ende , max); 
    }    
}