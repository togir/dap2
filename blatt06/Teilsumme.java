/**
 * Teilsumme
 */
public class Teilsumme {
    public static void main(String[] args) {
        
    }
    int anfang,ende;
    long sum;
    public Teilsumme(int start , int end , long summe ){
        anfang =  start ; 
        ende = end; 
        sum = summe ; 
    }

    public int getStart() {
        return anfang;
    }

    public int getEnd() {
        return ende;
    }
    
    public long getSum(){
        return sum;
    }
    
    public String toString(){
        return "Start : "  + getStart() + ", Ende : " + getEnd() + ", Summe : " + getSum() ;
    }
}
