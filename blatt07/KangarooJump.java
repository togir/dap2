import java.util.Arrays;
import java.util.function.Function;

public class KangarooJump {

    private int targetPos; /* Target position of the kangaroo */
    private int maxPos; /* Highest index of a tile */
    private int maxJumps; /* Maximum number of jumps */
    private int[] jumpSizes; /* List of possible jump sizes on the given walkway */
    private Directions[][][] dynArr; /* Dynamic programming array to find the solution */

    public KangarooJump(int targetPos, int maxPos, int maxJumps) {
        this.targetPos = targetPos;
        this.maxPos = maxPos;
        this.maxJumps = maxJumps;
        initJumpSizes();
        /* Initialize whenever you think fits best */
        this.dynArr = null;
    }

    /* Compute a list of powers of 2 */
    private void initJumpSizes() {
        int maxJumpSizeIdx = 1;
        int maxPosCpy = maxPos;
        while (maxPosCpy > 0) {
            maxPosCpy = maxPosCpy >> 1;
            maxJumpSizeIdx++;
        }
        jumpSizes = new int[maxJumpSizeIdx];
        for (int i = 0; i < maxJumpSizeIdx; i++) {
            jumpSizes[i] = 1 << i;
        }
    }

    /* Initialize the dynamic programming array */
    private void initDynArray(){
        // ARR[Welcher Sprung][zu welcher position][geschwinigkeit]
        dynArr = new Directions[maxJumps][maxPos][jumpSizes.length];
    }

    /*
     * Fill the dynamic programming array completely or until
     * the target position has been reached.
     * dynArr[anzahl jump][position][speed]
     */
    private int fillDynArray() {
        /* TODO */
        /**
         * Fälle: 
         * [anzahljump] = 0
         */
        dynArr[0][0][0] = Directions.BOTH;
        dynArr[1][1][0] = Directions.LEFT;

        return -1;
    }

    /*
     * Finds a path from the filled dynamic programming array, assuming
     * that the target position has been reached on the specified jump.
     */
    private String findPath(int jump) {
        /* TODO */
        return null;
    }

    /*
     * Computes a String representing the path from position 0 to
     * the target position or null if non such path is possible.
     */
    public String computeDirections() {
        /* TODO */
        int jump = fillDynArray();
        return findPath(jump);
    }

    /*
     * Add a set of direction options to the options already
     * available in some cell of the dynamic programming array.
     */
    private void add(int x, int y, int z, Directions d) {
        dynArr[x][y][z] = dynArr[x][y][z].add(d);
    }

    public static void main(String[] args) {
        
        int ziel = -1;
        int wegEnde = -1;
        int jumps = -1;

        if(args.length == 3){
            try {
                ziel = Integer.parseInt(args[0]);
                wegEnde = Integer.parseInt(args[2]);
                jumps = Integer.parseInt(args[1]);

            } catch (NumberFormatException e) {
                System.out.println("FEHLER: Eines der mitgegebenen Argumente ist kein positiver Integer. ");
                System.exit(1);
            }
            
            if(ziel < 0 ){
                System.out.println("FEHLER: Der Zielindex darf nicht negativ sein. ");
                System.exit(1);
            }
            
            if(wegEnde < ziel){
                System.out.println("FEHLER: Die Länge des gesamten Weges darf nicht kleiner als das Ziel sein. ");
                System.exit(1);
            }

            if(jumps < 1) {
                System.out.println("FEHLER: Es muss mindestens 1 Sprung erlaubt sein!");
                System.exit(1);
            }
            
            KangarooJump KG = new KangarooJump(ziel, wegEnde, jumps);
            KG.toString();
            
        }
        else{
            System.out.println("FEHLER: Es müssen genau 3 Argumente mitgegeben werden.");
            System.exit(1);
        }

    }

    /*
     * Enum to help you keep track of direction options in
     * the dynamic programming array.
     */
    public static enum Directions {
        NONE('0'), LEFT('-'), RIGHT('+'), BOTH('x');

        /* Display character for printing */
        private char s;

        Directions(char s) {
            this.s = s;
        }

        /* Checks if left is a viable option */
        public boolean hasLeft() {
            return this == LEFT || this == BOTH;
        }

        /* Checks if right is a viable option */
        public boolean hasRight() {
            return this == RIGHT || this == BOTH;
        }

        /* Adds a given set of direction options and returns the result */
        public Directions add(Directions other) {
            if (this == NONE || other == BOTH)
                return other;
            if (other == NONE || this == BOTH)
                return this;
            if ((this == LEFT && other == RIGHT) || (this == RIGHT && other == LEFT))
                return BOTH;
            return this;
        }

        @Override
        public String toString() {
            return "" + s;
        }
    }
}