
g(i, j) i = welches Feld will ich erreichen, j = wie viel Sprünge kann ich noch machen.

für den fall 0 sprünge übrig und i != 0
    -> i kann nicht erreicht werden
    -> return infnity

für den fall 1 Sprung übrig und i = 1
    -> return 1, da immer ein Sprung auf das erste Feld gemacht wird.

für den fall >1 Sprünge übrig
    -> return min({g(i', j-1)} | Erreicht i in einem Sprung) +1, infinity)