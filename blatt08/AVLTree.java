public class AVLTree<T extends Comparable<T>> { // AVL-Eigenschaften : wenn für jeden Knoten gilt, dass die Höhe seines linken 
                                                // und die seines rechten Teilbaums sich höchstes um 1 unterscheiden.
    private AVLTree<T> lc;
    private AVLTree<T> rc;
    private AVLTree<T> p;
    private T key ;
    private int height;
    private AVLTree<T> root;

    public AVLTree(T[] array){ // Aus einem Array einen AVL-Baum machen
        for (int i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        this.height = this.height();
    }

    public AVLTree (T key) { // Konstruktor falls key die Wurzel wird
        this.key = key;
    }

    public T getKey(){ // Rückgabe key 
        return this.key;
    }
    
    // Es darf keinen Baum ohne key geben deshalb ist dieser Konstruktor 
    // nur für die Interne verwendung
    private AVLTree () { // Erstellt leeren AVL-Baum
        this.lc = null;
        this.rc = null;
        this.p = null;
        this.height = 0;
        this.key = null;
        this.root = this;
    }
    
    public void add(T value){
        if(this.root == null ){ // Fügt Element als Wurzel ein wenn Baum leer
            root = new AVLTree<T>(value);
        }
        if( this.key== null){ // Rekursionsabbruch , einzufügender Key wird eingefügt
            System.out.println("Füge " + value + " in AVL-Baum ein");
            this.key = value;
            this.height = 0;
        }
        else if ( this.getKey().compareTo(value) > 0 ){ // Wenn das derzeitige Element größer ist als das einzufügende Element
            if(this.lc == null) {                       // Dann wird add auf den linken Teilbaum aufgerufen und die richtige Position ermittelt
                this.lc = new AVLTree<T>();
            }
            lc.add(value);
            lc.p = this;
        }
        else { // Wenn das derzeitige Element kleiner als das einzufügende Element -> add () rechten Teilbaum
            if (this.rc == null ){ // Neuer leerer Knoten / Baum wird erstellt falls der rechte Baum leer ist um das Element einzufügen
                this.rc = new AVLTree<T>();
            } 
            rc.add(value); // wenn das Element nicht eingefügt werden kann dann muss man weiter runter 
            rc.p = this;
        }
            this.height = this.height(); // Anpassung der Höhe 
            this.balance(); // Baum wird balanciert um die AVL-Eigenschaften Einzuhalten 
            assert assertBalenced() : "Fehler Baum ist nicht ausgeglichen";
    }    
    
    public void rotateRight(){                              //      a                            b
        if(this.lc != null && this.lc.rc != null){          //     / \        RECHTSROT.       /  \
        System.out.println("Führe Links-Rotation aus");     //    b   c         -->           d    a
        AVLTree<T> x = this.lc;                             //   / \                              / \
        AVLTree<T> z = x.rc;                                //  d   e                            e    c
        x.rc = this;
        this.lc = z;
        this.height = this.height();
        x.height = this.height() ;
        }
    }
    public void rotateLeft(){                                   //          y                           y
        if(this.rc != null && this.rc.lc != null){              //         / \      LINKSROT.          / \
            System.out.println("Führe Rechts-Rotation aus");    //        x   c     -->               z   C
        AVLTree<T> y = this.rc;                                 //       / \                         / \
        AVLTree<T> z = y.lc;                                    //      A   z                       x   B1
        y.lc = this;                                            //          /\                     /\
        this.rc = z;                                            //         B0 B1                  A  B0
        
        this.height = this.height(); 
        y.height =  y.height(); 
        }
        
    }

    private int height() { // Bei jedem rekursiven Aufruf wird +1 gerechnet und die rekursiven 
                           // Aufrufe der RightChilds und LeftChilds werden miteinander verglichen.
        if(this.key == null) {
            return 0;
        }
        
        if(this.lc == null && this.rc == null ) {
            return 1;
        } 
        if(this.lc == null && this.rc != null) {
            return this.rc.height() +1;
        }
        if(this.rc == null && this.lc != null) {
            return this.lc.height() +1;
        }
        return Math.max(this.lc.height(), this.rc.height()) +1;
    }
    
    public void balance(){
        if(this.lc == null && this.rc != null && rc.height() > 1) {  // Fall wenn der Teilbaum des rightChilds hat größeren Höhenunterschied als 1 zum leftChild Teilbaum
            System.out.println("Rechter Teilbaum von " + this.key + " hat Hoehe " +  this.rc.height() + "Linker Teilbaum hat Hoehe 0" );
            rc.rotateLeft();
        }
        if(this.lc != null && this.rc == null && lc.height() > 1){  // Fall wenn der Teilbaum des leftChilds hat größeren Höhenunterschied als 1 zum rightChild Teilbaum
            System.out.println("Rechter Teilbaum von " + this.key + " hat Hoehe 0 Linker Teilbaum hat Hoehe " + this.lc.height() );
            lc.rotateRight();
        }
        
        if(this.lc != null && this.rc !=null){ 
            
            if(this.lc.height() > this.rc.height() +1){  // Fall wenn Doppelrotation nötig ist , wenn von dem Linken baum der rightChild einen  Höhenunterschied größer gleich 1 hat zu seinem Linken Nachbarn hat
                System.out.println("Rechter Teilbaum von " + this.key + " hat Hoehe " +  this.rc.height() + "Linker Teilbaum hat Hoehe " + this.lc.height() );
                if(this.lc.lc.height() < this.rc.lc.height()){
                    lc.rotateLeft();
                }
                this.rotateRight();
            }
            else if(this.rc.height() > this.lc.height() +1){ // Fall wenn Doppelrotation nötig ist , wenn von dem Rechten baum der leftChild einen  Höhenunterschied größer gleich 1 hat zu seinem rechten Nachbarn hat
                System.out.println("Rechter Teilbaum von " + this.key + " hat Hoehe " +  this.rc.height() + "Linker Teilbaum hat Hoehe " + this.lc.height() );
                if(this.rc.rc.height() > this.lc.rc.height() +1){
                    rc.rotateRight();
                }
                this.rotateLeft();
            }
        }

    }

    public void PreOrder(){
        // Preoder -> Root, LeftChild, RightChild
        System.out.print(this.key + " ");
        if(this.lc != null) {
            this.lc.PreOrder();
        }
        if(this.rc != null) {
            this.rc.PreOrder();
        }
    
    }
    public void postOrder(){
        // PostOrder-> LeftChild, Rightchild, Root
        if (this.lc != null){
            this.lc.postOrder();
        }
        if(this.rc != null){
            this.rc.postOrder();
        }
        System.out.print(this.key + " ");
    }
    
    public void inOrder(){
        // InOrder -> LeftChild, Root, RightChild
        if (this.lc != null){
            this.lc.inOrder();
        }
        System.out.print(this.key + " ");
        if(this.rc != null){
            this.rc.inOrder();
        }
    }
    
    public int  getHeight() {
        return this.height();
    }

    private boolean assertBalenced() {

        if(this.lc == null && this.rc == null ) {
            return true;
        } 
        if(this.lc == null && this.rc != null) {
            return this.rc.getHeight() < 2;
        }
        if(this.rc == null && this.lc != null) {
            return this.lc.height() < 2;
        }
        return Math.abs(this.lc.height() -this.rc.height()) < 2;
    }
    
}