public class SearchTree<T extends Comparable<T>> {
    private SearchTree<T> lc;
    private SearchTree<T> rc;
    private T key ;
    private int height;

    public SearchTree(T[] array){
        for (int i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        this.height = this.height();
    }

    public SearchTree (T key) {
        this.key = key;
    }

    // Es darf keinen Baum ohne key geben deshalb ist dieser Konstruktor 
    // nur für die Interne verwendung
    private SearchTree () {
        this.lc = null; // Attribute für rechtes Kind und linkes Kind
        this.rc = null;
        this.height = 0;    // Leerer Baum hat höhe 0
        this.key = null;    // und keinen Key
    }

    public void add(T element) {
        if(this.key == null) {  // Wenn der Baum leer ist dann wird das Element ohne weiteres 
            this.key = element; // In der Wurzel eingefügt 
            System.out.println( element + " in der wurzel eingefügt");
            return;
        }
        
        // TODO was passiert wenn compareTo == 0 
        if(this.key.compareTo(element) < 0) { 
            
            // In den rechten Teilbaum einfügen falls element größer als der aktuelle key ist
            // ansonten in den linken Teilbaum einfügen.
            // Wenn kein rechten Teilbaum vorhanden ist wir ein neuer erzeugt. Ansonste wird
            // die add Methode rekursiv aufgerufen auf das linke kKind bis ganz unten sind.
            
            if(rc == null) {
                rc = new SearchTree<T>(element);
            } else {
                rc.add(element);
            }
        } else {

            System.out.println(element + " ist kleiner als " + this.key );
            // wenn das derzeitige Element im Baum kleiner als der einzufügende Key ist dann gehen wir in den linken Teilbaum
            // Wenn kein linker Teilbaum vorhanden ist wir ein neuer erzeugt. Ansonste wird
            // die add Methode rekursiv aufgerufen.
            if(lc == null) {
                lc = new SearchTree<T>(element);
            } else {
                lc.add(element);
            }
        }
        this.height = this.height();
    }

    public int getHeight() { // Methode für die rückgabe der Höhe
        return this.height;
    }

    public void PreOrder(){
        // Preoder -> Root, LeftChild, RightChild
        System.out.print(this.key + " ");
        if(this.lc != null) {
            this.lc.PreOrder();
        }
        if(this.rc != null) {
            this.rc.PreOrder();
        }
    
    }
    public void postOrder(){
        // PostOrder-> LeftChild, Rightchild, Root
        if (this.lc != null){
            this.lc.postOrder();
        }
        if(this.rc != null){
            this.rc.postOrder();
        }
        System.out.print(this.key + " ");
    }
    
    public void inOrder(){
        // LeftChild , Root , Rightchild
        if (this.lc != null){
            this.lc.inOrder();
        }

        System.out.print(this.key + " ");
        if(this.rc != null){
            this.rc.inOrder();
        }
    }

    private int height() { // Bei jedem rekursiven Aufruf wird +1 gerechnet und die rekursiven 
                        // Aufrufe der RightChilds und LeftChilds werden miteinander verglichen.
        if(this.key == null) {
            return 0;
        }
        
        if(this.lc == null && this.rc == null ) {
            return 1;
        } 

        if(this.lc == null && this.rc != null) {
            return this.rc.height() +1;
        }
        if(this.rc == null && this.lc != null) {
            return this.lc.height() +1;
        }

        return Math.max(this.lc.height(), this.rc.height()) +1;
    }
}