import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;
import java.io.IOException;

public class SearchTreeApplication {

    public static void main(String[] args) {

        if(args.length < 1 ) {
            System.out.println("FEHLER: Es muss mindestens 1  Parameter angegen werden!");
            System.exit(1);
        }
        
        if(args.length > 2) {
            System.out.println("FEHLER: Es dürfen maximal 2  Parameter angegen werden!");
            System.exit(1);
        }
        
        boolean aus = false;
        if(args.length == 2 ){
            if(args[1] != null){
                aus = true;
            }
        }
        String path = args[0];

        ArrayList<ArrayList<Integer>> valuesList = parseCsv(path);
        
        ArrayList<SearchTree<Integer>> treeList = new ArrayList<SearchTree<Integer>>();
        
        for (ArrayList<Integer> values : valuesList){

            Integer [] arr1 = new Integer [values.size()];
            for(int i = 0 ; i < values.size(); i++){
                arr1[i] =  values.get(i);
            }
            treeList.add(new SearchTree<Integer>(arr1));
        }

        if(aus){
            for (int i = 0; i <treeList.size(); i++) {
                System.out.println("Hoehe: " + treeList.get(i).getHeight());

                if(args[1].equals("post")){         // wird gelesen wie der Baum ausgegeben werden soll
                    treeList.get(i).postOrder();
                }
                else if(args[1].equals("pre")) {
                    
                    treeList.get(i).PreOrder();
                    
                }
                else if(args[1].equals("in")){
                    
                    treeList.get(i).inOrder();
                    
                }
                else{
                    System.out.println("FEHLER: Der 2. mitgegebene Parameter entspricht nicht den Vorgaben, mögliche Eingaben sind : in / post / pre ");
                }
                System.out.println();
            }
        }
        else{                           // Ausgabe wenn kein Parameter zur Ausgabe mitgegeben wurde 
            for (int i = 0; i <treeList.size(); i++) {
                System.out.println("Hoehe: " + treeList.get(i).getHeight());
                treeList.get(i).inOrder();
                System.out.println();         
            }
        }
        
        }
        
    
    

    private static ArrayList<ArrayList<Integer>> parseCsv(String path) {
        try {
            BufferedReader file = new BufferedReader( new FileReader( path ) );
            
            ArrayList<ArrayList<Integer>> fileList = new ArrayList<ArrayList<Integer>>();
            String zeile = null;

            while((zeile = file.readLine()) != null) {

                ArrayList<Integer> rowList = new ArrayList<Integer>();
                
                StringTokenizer st = new StringTokenizer(zeile,",");

                while (st.hasMoreTokens()) {
                    rowList.add(Integer.parseInt( st.nextToken()));
                }

                fileList.add(rowList);
            }

            file.close();

            return fileList;
        } catch (IOException e){
            System.out.println("FEHLER: Die Datei kann nicht gelesen werden!");
            System.exit(1);
        } catch(IllegalArgumentException e ) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden!");
            System.exit(1);
        } catch(NoSuchElementException e) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden!");
            System.exit(1);
        } catch(NullPointerException e) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden!");
            System.exit(1);
        }

        return new ArrayList<ArrayList<Integer>>(); // Weil es halt java ist.... 
    }
}