public class MinHeap {
    
    private HeapElement [] arr ;
    private int[] idToIndex;
    private int maxJobId;
    private int size;

    public MinHeap(int n , int k ){
        assert n < 1 || k < 1 : "FEHLER: Es können nicht weniger als 1 Element im Heap sein oder Job-IDs erstellt werden.";
        this.maxJobId = k;
        this.arr = new HeapElement[n +1];
        this.arr[0] = new HeapElement(Integer.MAX_VALUE, k+1);
        this.idToIndex = new int[k+1];
    }

    private int parent(int pos) { 
        return pos / 2; 
    } 

    private int leftChild(int pos) { 
        return (2 * pos); 
    }

    private int rightChild(int pos) {
        int value = (2 * pos) + 1;
        if(arr[value] == null) {
            return 0;
        } 
        return value;
    }

    private boolean isLeaf(int pos) { 
        if (pos >= (size / 2) && pos <= size) { 
            return true; 
        } 
        return false; 
    }

    private void swap(int parent, int child) {
        if(size < 2){
            throw new IllegalStateException("FEHLER: Es befinden sich weniger als 2 Elemente im Heap");
        }
        // Elemente in Heap und in idToIndex tauschen

        // In heap Tauschen
        HeapElement tmp = arr[parent];
        arr[parent] = arr[child];
        arr[child] = tmp;

        // In Lookup-Table tauschen
        int tmp2 = idToIndex[parent];
        idToIndex[parent] = idToIndex[child];
        idToIndex[child] = tmp2;
        
    }

    private void heapify(int i) {
        
        if(!isLeaf(i)) {
            if(
                arr[i].getPrio() > arr[leftChild(i)].getPrio() ||
                arr[i].getPrio() > arr[rightChild(i)].getPrio()
            ) {
                // Größtes Element der Kinder suchen
                if(arr[leftChild(i)].getPrio() <= arr[rightChild(i)].getPrio()) {
                    swap(i , leftChild(i));
                    heapify(leftChild(i));
                } else {
                    swap(i , rightChild(i));
                    heapify(rightChild(i));
                }
            }
        }
    }

    public void insert( HeapElement element) {
        if(size >= arr.length) {
            throw new IllegalStateException("FEHLER: Element kann nicht eingefügt werden");
        }

        size = size +1;

        arr[size] = element;
        idToIndex[element.getID()] = size;

        int current = size;
        
        while(current > 1 && arr[current].getPrio() < arr[parent(current)].getPrio()) {
            swap(current, parent(current));
            current = parent(current);
        }
        
        for (int i = 0; i < idToIndex.length; i++) {
            if( idToIndex [i] > current){
                idToIndex[i] ++;
            }
        }
    }

    public void remove(int jobID){
        assert size < 1 : "FEHLER: Kein Element vorhanden!" ;
        
        // Wenn das letzte gelöscht wird
        if(idToIndex[jobID] == size) {
            System.out.println("moiin");
            arr[idToIndex[jobID]] = null;
            idToIndex[jobID] = 0;
            return;
        }

        /**
         * Das element mit der jobID mit dem Letzten Tauschen 
         *  -> Letztes entfernen
         *  -> Schauen ob das getauschte element weiter hoch/ runter muss
         */

        int element = idToIndex[jobID];
        System.out.println(element + "<- index nach jobid");
        swap(element, size);
        arr[size] = null;
        idToIndex[jobID] = 0;

        // Wenn ein Elternelement größer ist muss das element weiter hoch
        while(arr[parent(element)].getPrio() > arr[element].getPrio()){
            System.err.println("Element hoch");
            if(element == 1) {
                break; // Abbruchbedingung wird sind ganz oben
            }
            swap(parent(element), element); // Hochschieben
        }
        decrementIdToIndex(element);
        heapify(element);


    }

    private void decrementIdToIndex(int v) {
        for (int i = 0; i < idToIndex.length; i++) {
            if(idToIndex[i] > v) {
                idToIndex[i] = idToIndex[i] -1;
            }
        }
    }


    public HeapElement extractMin() {
        swap(1, size);
        idToIndex[arr[1].getID()] = 0;
        HeapElement el = arr[size];
        arr[size] = null;
        decrementIdToIndex(1);
        heapify(1);
        return el;
    }

    public void decreasePrio(int jobID , int prio){
        if(arr[idToIndex[jobID]].getPrio() <= prio ){
            throw new IllegalArgumentException("Fehler: Die gewünschte Priorität ist ungültig");
        }
        
        arr[idToIndex[jobID]] = new HeapElement(prio, jobID);

        int element = idToIndex[jobID];
        while ((arr[leftChild(element)].getPrio() < arr[element].getPrio() || arr[rightChild(element)].getPrio() < arr[element].getPrio()) &&( element > 0 && element < size )){
            if(arr[rightChild(element)].getPrio() < arr[leftChild(element)].getPrio()){
                swap( element , rightChild(element));
                element = rightChild(element);
            }
            else{
                swap(element, leftChild(element));
                element = leftChild(element);
            }
        }
    }

    public void printHeap(){

        for (int i = 1; i < arr.length; i++) {
            if(arr[i] == null) {
                break;
            }
            System.out.print("(" + arr[i].getPrio() + ", " + arr[i].getID() + ")" );


        }
        System.out.println();

        for (int i = 1; i < idToIndex.length; i++) {
            System.out.print(idToIndex[i] + ", ");

        }
        System.out.println();
    }
}