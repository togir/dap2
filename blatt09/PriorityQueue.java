import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;
import java.io.IOException;

public class PriorityQueue {
    public static void main(String[] args) {
        int maxl = -1;
        int maxJ = -1;
        if(args.length != 3) {
            System.out.println("FEHLER: Es sind nicht EXAKT DREI!1elf!! Kommandozeilenparameter");
            System.exit(1);
        }
        try {
            maxl = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println("FEHLER: Die maximale Größe des zu erzeugenden Heaps muss >= 1 sein ");
            System.exit(1);
        }
        try {
            maxJ = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("FEHLER: Die höchste Job-ID");
            System.exit(1);
        }

        MinHeap heap = new MinHeap(maxl, maxJ);
        // Nice

        parseCsv(args[2], heap);

        
    }

    private static void parseCsv(String path, MinHeap heap) {
        try {
            BufferedReader file = new BufferedReader( new FileReader( path ) );
            
            String zeile = null;


            while((zeile = file.readLine()) != null) {

                
                StringTokenizer st = new StringTokenizer(zeile,",");
                String cmd  = st.nextToken();
                if(cmd.equals("A")) {
                    
                    int prio = Integer.parseInt(st.nextToken());
                    int jobID = Integer.parseInt(st.nextToken());
                    
                    try {
                        HeapElement el = new HeapElement(prio, jobID);
                        heap.insert(el);
                        System.out.println("Element (" + el.getPrio() + ", " + el.getID() +") wurde zum Heap hinzugefuegt");

                    } catch (IllegalArgumentException e) {
                        System.out.println(e);
                        System.exit(1);
                    }

                } else if(cmd.equals("R")) {
                    int jobID = Integer.parseInt(st.nextToken());
                    System.out.println("Der Job mit der ID " + jobID + " wurde entfernt");
                    heap.remove(jobID);

                } else if(cmd.equals("E")) {
                    HeapElement el = heap.extractMin();
                    System.out.println("Element (" + el.getPrio() + ", " + el.getID() + ")"  + " wurde extrahiert");
                    
                }else if(cmd.equals("D")){

                    int jobID = Integer.parseInt(st.nextToken());
                    int prio = Integer.parseInt(st.nextToken());
                    try {
                        heap.decreasePrio(jobID, prio);
                        System.out.println("Der Prioritaetswert von Job " + jobID +" wurde auf " + prio + " reduziert");
                    } catch (IllegalArgumentException e) {
                        System.err.println(e);
                        System.exit(1);
                    }
                }else{
                    System.out.println("FEHLER: Der Eingegebene Buchstabe in der CSV-Datei entspricht nicht den Vorgaben (A,R,E,D)"); 
                    System.exit(1);
                }
                
                heap.printHeap();

            }
            
            file.close();

        } catch (IOException e){
            System.out.println("FEHLER: Die Datei kann nicht gelesen werden! IOException");
            System.exit(1);
        }
        catch(NumberFormatException e){
            System.out.println("FEHLER: Die Zahl der eingegebenen Datei entspricht nicht den Vorgaben");
            System.exit(1);
        }
        catch(IllegalArgumentException e ) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden! IllegalArgumentException");
            System.exit(1);
        } catch(NoSuchElementException e) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden! NoSuchElementException");
            System.exit(1);
        } catch(NullPointerException e) {
            System.out.println("FEHLER: Die Datei konnte nicht gelesen werden! NullPointerException ");
            System.exit(1);
        }
        
    }
}