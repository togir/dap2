import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.print.attribute.standard.Media;

public class BTree implements Serializable {

    /* Versionsnummer für Serialisierung */
    private static final long serialVersionUID = 1L;

    /* Zählvariable für IDs */
    private static int COUNTER = 0;

    /* Zählvariablen für Festplattenzugriffe */
    public static int READ_OPS = 0;
    public static int WRITE_OPS = 0;

    /* Attribute */
    private int id;
    private int t;
    private Integer[] children;
    private Integer[] keys;
    private int nKeys;
    private boolean isLeaf;

    /* Konstruktor der Standardwerte anlegt und auf die Festplatte speichert */
    public BTree(int t) throws IOException {
        id = COUNTER++;
        this.t = t;
        children = new Integer[2 * t];
        keys = new Integer[2 * t - 1];
        nKeys = 0;
        isLeaf = true;
        save();
    }

    /* Druckt die Struktur des BTree in die Standardausgabe */
    public void printStructure() throws IOException, ClassNotFoundException {
        if(this.isLeaf){
            System.out.print("Knoten " + id + " ist ein Blatt mit " + nKeys + "Werten  ");
            return;
        }
        int kinder = 0 ; 
        for (int i = 0; i <= nKeys; i++) {
            if(this.children[i] != null) {
                kinder ++;
            }
        }
        // Anzeigen des Knotens und Anzahl / ID der Kinder
        System.out.print("Knoten " + id + " hat " + kinder + " Kinder: ");
        for (int i = 0; i <= nKeys; i++) {
            if(this.children[i] != null) {
                System.out.print(this.children[i]);
            }
        }
    
        // Eine Schleife die durch alle Kinder läuft
        for (int i = 0; i <= nKeys; i++) {
            // Rekuriver aufruf
            if(this.children[i] != null){
                BTree tmpTree = BTree.read(this.children[i]);
                tmpTree.printStructure();
            }
        }
        
    }

    /* Druckt alle Schlüssel im BTree inorder mit Leerzeichen getrennt in die Standardausgabe */
    public void printInOrder() throws IOException, ClassNotFoundException {
        
        // Bei einem Leaf gibt es keine Kinder, deshalb werden nur die Keys ausgegeben.
        if(isLeaf) {
            for (int i = 0; i < nKeys; i++) {
                // Rekuriver aufruf
                System.out.print(this.keys[i] + ", ");
            }
            return; // Abbrechen
        }


        /**
         * Reihenfolge der Ausgabe
         *  [ 2, 4, 6, 8, 10,]          
         *   /  |  |  |  |  \
         *  1   3  5  7  9   11
         */ 

        for (int i = 0; i < nKeys; i++) {
            // print child 
            if(this.children[i] != null){
                BTree tmpTree = BTree.read(this.children[i]);
                tmpTree.printInOrder();
            }

            // print key
            System.out.print(this.keys[i] + ", ");        
        }

        // Am ende der Schleife wurder der Child rechts vom keys[nKeys -1] nicht ausgegeben.
        if(this.children[nKeys] != null) {
            BTree tmpTree = BTree.read(this.children[nKeys]);
            tmpTree.printInOrder();
        }

    }   

    /* 
     * Aufruf an die Wurzel um einen Schlüssel k hinzuzufügen.
     * Gibt die nach Abschluss der Operation aktuelle Wurzel zurück.
     */
    public BTree rootInsert(int k) throws IOException, ClassNotFoundException {

        /* TODO keys nicht doppelt einfügen  */

        if(this.isLeaf) {

            // Wuzel ist der einige Knoten und hat noch platz für ein extra Element

            if(this.nKeys < this.keys.length-1) {
                int i = 0 ; 
                while (this.keys[i] < k ){
                    i ++; 
                }
                //shift key array one to the right to make space for the new key k
                shiftRight(this.keys, i);
                this.keys[i] = k; 
                this.nKeys = this.nKeys+1;
                
                return this;
            }

            
            // Wuzel ist der einige Knoten und hat keinen platz für ein extra Element
            // Median wird neuen Wurzel
            int median = 0;
            if(keys.length % 2 == 0 ){
                median = keys[keys.length / 2 ];
            }
            else {
                median = keys[(keys.length / 2) +1 ]; 
            }

            int medianValue = this.keys[median];

            BTree newRoot = new BTree (this.t) ;
            newRoot.insert(keys[median]);

            // rechts von median wird RC , Links vom median LC
            BTree leftTree = new BTree(this.t);

            // Alle Elemente Links von median in den neuen Child einfügen
            leftTree.nKeys = median;
            leftTree.keys = subArray(this.keys, 0, median-1);

            // Arrays des Rechten Childs anpassen
            this.nKeys = this.keys.length - (median +1);
            this.keys = subArray(this.keys, median+1, this.keys.length -1);


            // ChildTress an Wurzel anhängen
            newRoot.children[0] = leftTree.id;
            newRoot.children[1] = this.id;
            newRoot.isLeaf = false;

            // Element Links oder rechts der Wurzel einfügen
            if(k > medianValue) {
                this.rootInsert(k);
            } else {
                leftTree.rootInsert(k);
            }
        }
        
        // Wurzel ist nicht der Einzigen Knoten
        // Rausfinden welchen
        
        
        
        
        
        
    }                   

    // Das letzte Element des Arrays wird entfernt und alle Elemente ab und inlusive i werden 
    // 1 nach rechts verschoben. An Position i wird null eingefügt
    private void shiftRight(Integer[] arr, int i ){

        if(i < 0 ||i >= arr.length)
            throw new IllegalStateException("Fehler: Index ist ungültig");

        if(i > arr.length -1) 
            throw new IllegalStateException("Fehler: Array kann nicht nach rechts verschoben werden. Es ist nicht genug Platz vorhanden");
        
        // Vom vorletzten Element des Arrays alle Elemente einen nach rechts verschieben bis
        // der Zähler bei i angekommen ist.

        for (int j = arr.length -2; j >= i; j--) {
            arr[j+1] = arr[j];
        }
        arr[i] = null;

    }

    // Der Index i wird entfert und alle Elemente nach dem Index i werden um eine Stelle nach
    // links verschoeben. Am ende des Arrays wir 1 eingefügt.
    private void shiftLeft(Integer[] arr, int i) {
        
        if(i < 0 ||i >= arr.length)
            throw new IllegalStateException("Fehler: Index ist ungültig");


        // Von Punkt i aus alle nach Links schieben. Am ende wird null angehängt
        for(; i < arr.length - 1; ++i) {
            arr[i] = arr[i+1];
        }
        arr[arr.length-1] = null;
    }


    /* Rekursiver Aufruf um einen Schlüssel k in den BTree hinzuzufügen */
    private void insert(int k) throws IOException, ClassNotFoundException {
        /* TODO */
    }


    /* Teilt den i-ten Kindknoten und gibt den dabei neu erstellten Geschwisterknoten zurück */
    private BTree split(int i, BTree child) throws IOException {
        /* TODO */
        return null;
    }

    /* 
     * Aufruf an die Wurzel um einen Schlüssel k zu löschen.
     * Gibt die nach Abschluss der Operation aktuelle Wurzel zurück.
     */
    public BTree rootDelete(int k) throws IOException, ClassNotFoundException {
        /* TODO */
        return null;
    }

    /* Rekursiver Aufruf um einen Schlüssel k aus dem BTree zu löschen */
    private void delete(int k) throws IOException, ClassNotFoundException {
        /* TODO */
    }

    /**
     * Sucht den Knoten, der einen key k enthalten sollte.
     * @param k
     * @param node
     * @return Btree
     * @throws IOException
     * @throws ClassNotFoundException
     */

    /* Schreibt aktuellen Zustand auf die Festplatte */
    public void save() throws IOException {
        String filepath = filename();
        FileOutputStream fileOut = new FileOutputStream(filepath);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
        objectOut.writeObject(this);
        objectOut.close();
        WRITE_OPS++;
    }

    /* Liest letzten gesicherten Zustand eines Knoten von der Festplatte */
    public static BTree read(int id) throws IOException, ClassNotFoundException {
        String filepath = filename(id);
        FileInputStream fileIn = new FileInputStream(filepath);
        ObjectInputStream objectIn = new ObjectInputStream(fileIn);
        BTree ret = (BTree) objectIn.readObject();
        objectIn.close();
        READ_OPS++;
        return ret;
    }

    /* Übersetzt eine ID in einen Dateinamen */
    private static String filename(int id) {
        return id + ".node";
    }

    /* Abkürzung, um den Dateinamen eines Knoten direkt zu bestimmen */
    private String filename() {
        return BTree.filename(this.id);
    }

    // Prüft ob ein Elemen in einem Array ist
    private static boolean contains(Integer[] arr, int i) {
        for (Integer integer : arr) {
            if(integer == i) {
                return true;
            }
        }

        return false;
    }

    // Alle Elemente des Arrays werden von a..b auf 0..b-a geschrieben.
    // Die restlichen Stellen werden mit null aufgefüllt
    private Integer[] subArray(Integer[] arr, int a, int b) throws IllegalStateException {
        
        if(a < 0 || b < 0 || a > arr.length || b > arr.length || a > b) {
            throw new IllegalStateException("Fehler: Invalide parameter für ein Teilarray!");
        }
        
        Integer[] subArr = new Integer[arr.length];
        
        for (int i = 0; i < subArr.length; i++) {
            if(a+i < b) {
                subArr[i] = arr[a+i]; 
            } else {
                subArr[i] = null;
            }
        }

        return subArr;
    }

    // Löscht alle werte die null sind und shiftet das Arrays
    
}