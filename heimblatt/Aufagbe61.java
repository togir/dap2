import java.lang.reflect.Array;
import java.util.Arrays;

public class Aufagbe61<T extends Comparable<T>> {
    private T content;
    private Aufagbe61<T> leftChild, rightChild;

    public Aufagbe61() {
        content = null;
        leftChild = null;
        rightChild = null;
    }
    public void PrintPreeOrder() {
        if(!isEmpty()) {
            System.out.println(content + " , ");
            leftChild.PrintPreeOrder();
            rightChild.PrintPreeOrder();
        }
    }

    public void printInOrder() {
        if(!isEmpty()) {
            leftChild.printInOrder();
            System.out.println(content);
            rightChild.printInOrder();
        }
    }

    public static void main(String[] args) {
        Aufagbe61<Integer> tree = new Aufagbe61<Integer>();
        int[] array = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        marvin(array, tree);
        tree.postorder();
        System.out.println("-------------");
        System.out.println(tree.high());
    }
    public int high()
    {
        if(isEmpty()) {
            return 0;
        }

        if(isLeaf()){
            return 0;
        }

        if(leftChild != null && rightChild == null) {
            return leftChild.high()+1;
        }

        if (leftChild == null && rightChild != null) {
            return rightChild .high();
        }

        return Math.max(this.leftChild.high(), this.rightChild.high()) +1;
        
    }
    public void postorder() {
        if(!isEmpty()){
            leftChild.postorder();
            rightChild.postorder();
            System.out.print(content + " , ");
        }
    }

    public T getContent() {
        if (!isEmpty()) {
            return content;
        } else {
            throw new RuntimeException();
        }
    }

    public boolean isEmpty() {
        return content == null;
    }

    public boolean isLeaf() {
        return !isEmpty() && leftChild.isEmpty() && rightChild.isEmpty();
    }

    public void add(T t) {
        if (isEmpty()) {
            content = t;
            leftChild = new Aufagbe61<T>();
            rightChild = new Aufagbe61<T>();
        } else {
            if (content.compareTo(t) > 0) {
                leftChild.add(t);
            } else if (content.compareTo(t) < 0) {
                rightChild.add(t);
            }
        }
    }

    public boolean contains(T t) {
        if (isEmpty()) {
            return false;
        } else {
            if (content.compareTo(t) > 0) {
                return leftChild.contains(t);
            } else if (content.compareTo(t) < 0) {
                return rightChild.contains(t);
            }
            return true;
        }
    }

    public int size() {
        if (isEmpty()) {
            return 0;
        } else {
            return 1 + leftChild.size() + rightChild.size();
        }
    }

    

    public static int[] jorgo(boolean [] [] arr){
        int i = 0 ; 
        int j = 0;
        int counter = 0 ; 
        boolean promi = false ;
        int [] brr = {0,0};
        while ( !promi ){
            if ( counter == arr[i].length -1){
                break;
            }
            if ( j == arr[i].length -2 ){
                promi = arr[i][j] && arr[i][j+1];
                i++;
            }
            else{
                if(!arr[i][j]){
                    i++;
                    j=0;
                }
                else{
                    counter ++;
                    j++;
                }
            }
        }
        brr[0]= i;
        brr[1] = j;
        return brr;
    }











    
    public void show() {
        if (!isEmpty()) {
            leftChild.show();
            System.out.println(content);
            rightChild.show();
        }
    }

    public static void marvin(int [] arr, Aufagbe61<Integer> t){ 
        if(arr.length <= 0) {
            return;
        }

        int m = arr.length /2;
        t.add(arr[m]);

        if(m > 0) {
            int[] n = Arrays.copyOfRange(arr, 0, m);

            marvin(n, t);
        }

        if(m < arr.length-1) {
            int[] c = Arrays.copyOfRange(arr, m+1, arr.length);
            marvin(c, t);
        }
        }
    }
/**
 * marvin(A , T )
 * if length(A) > 0
 * |     
 * |    m = length(A) / 2 
 * |    einfügen(A[m], T)
 * |
 * |    if  m > 0 do 
 * |    |   N <- A[1 , ... , m-1] 
 * |    |   marvin(N, T)
 * |    
 * |    if m < lenght(A)-1
 * |    |   C <- A[m+1...lenght(A)]
 * |    |   marvin(C, T)
 * |
 * Beschreibung:
 * 
 * Die Funktion fügt in jedem Durchlauf das mittleres Elemente des Teilfeldes ein.
 * Nach jedem Durchlauf halbiert sich das Teilfeld und so werden Elemente der linken hälfte eingefügt
 * und dann der rechten hälfte.
 * Da das Eingabe Array schon vorsortiert ist wird der Baum so gefüllt, dass erst alle Elemente der
 * Ebene i-1  zwei Kinder auf der Ebene i haben bevor die Ebene i+1 befüllt wird.
 * 
 * 
 * Laufzeit:
 * O(n*logn), d jedes element einmal eingefügt wird und einfügen in einen Baum O(h) ist.
 *  Da h als vorrausetzung log(n) ist beträgt die Laufzeit O(n*log(n))......
 * 
 * Korrektheit
 * Der Algorithmus fügt am Anfang jedes Durchlaufs immer das mittlere Element ein.
 * Daraus folgt, dass Element ein rechtes und linkes kind hat , da es A[.. i-1 , i , i+1 ...]
 * außer im letzten Schritt kleinere & größere Zahlen gibt die als Kind angefügt werden.
 * Da die rekusiven Aufrufe jeweils mit einem Teilfeld Links [0...m-1] / rechts [m+1 .. n] ohne das bereits eingefügte
 * erfolgen, werden die felder immer kleiner sodass wir zu unserer Abbruchbedingung kommen.
 */


/**
 * Angenommen wir haben einen Binären Baum mit der 
 *           Wurzel A
 * Leftchild B         Rightchild C
 * 
 * 
 * Da der Preorder-durchlauf uns die Wurzel bestimmen lässt, (Wurzel, Links, Recht)
 * kann in dem Baum die Wurzel nicht getauscht werden ohne den PreOrder-Durlauf zu verändern.
 * (Alle 2 Fälle angeben??)
 * Fall 1:
 * 
 * 
 * Mit dem InOrder-durchlauf erhalten wir die Ausgabe (Links, Wurzel, Rechts).
 * Da wir durch den PreOrder durchlauf schon die Wurzel bestimmt haben können wir bestimmn ob
 * ein Element links oder rechts von der Wurzel liegt.
 *
 * Ein Element liegt links von der Wurzel, wenn es "vor" dem Wurzelelement aufgezählt wird. nach def. InOrder
 * 
 * Ein Element liegt Rechts vor der Wurzel wenn es "nach" dem Wurzelelement aufgezählt wird. nach def Inorder
 * 
 * Wenn nun Left- oder Rightchild vertauscht werden ändert sich auch der InOrder durchlauf.
 * 
 * => Wir können keine Elemente mit der Wurzel vertauschen ohne den PreOrder zu verändern.
 * => Wir können Left- Rightchild nicht vertauschen ohne den Inoder zu verändern.
 * 
 * ====> Da alle möglichen Fälle abgedeckt sind kann kein Element vertauscht werden ohne PreOrder oder InOrder
 * zu verändern.
 * 
 */
