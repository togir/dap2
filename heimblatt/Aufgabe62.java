import java.util.Arrays;
import java.util.Stack;
public class Aufgabe62 {
    public static void main(String[] args) {
        boolean [][] a = {
            {false,true,true,false,false,false},
            {false,false,true,true,true,false},
            {false,false,false,false,false,false}, // gesucht 2
            {false,false,true,false,false,true},
            {false, true, true, true, true, false},
            {false, false, true, false, true, false}
        };

        boolean [][] b = {
            {false, true, false, true, false},
            {false, false, false, true, false},
            {false, false, false, true, true},
            {false, false, false, true, false},// gesucht 3
            {true, true, false, true, false}
        };
        System.out.println(marvin(a));
        System.out.println(marvin(b));

        System.out.println(Arrays.toString(jorgo(a)));
        
    }



    public static int[] jorgo(boolean [] [] arr){
        int i = 0; 
        int j = 0;
        int counter = 0 ; 
        boolean promi = false;
        int [] brr = {0,0};
        while ( !promi ){
            if (i < arr.length ){
                break;
            }
            if ( j == arr[i].length -2 ){
                promi = arr[i][j] && arr[i][j+1];
                i++;
                counter = counter + 2 ;
            }
            if ( counter == arr[i].length -1){
                break;
            }
            else{
                if(!arr[i][j]){
                    i++;
                    j=0;
                    counter = 0 ;
                }
                else{
                    counter ++;
                    j++;
                }
            }
        }
        brr[0]= i;
        brr[1] = j;
        return brr;
    }

    public static int marvin(boolean[][] arr) {
        int n = arr.length;

        Stack<Integer> stack = new Stack<Integer>();

        for(int i = 0; i < n; i++) {
            stack.push(i);
        }

        while(stack.size() >1) {
            int a = stack.pop();
            int b = stack.pop();

            // Wenn Person a Person b kennt
            if(arr[a][b]) { 
                // Der Proimenten kennt niemanden deshalb 
                // kann a nicht der Promimente sein.
                stack.push(b);
            } else {
                stack.push(a);
            }
        }

        // letzetes Element vom Stack ist die einzige Möglichkeit
        // für einen promimenten, vorausgesetezt es gibt maximal
        // einen
        int c = stack.pop();

        // Kontrolle
        for(int i = 0; i < arr.length; i++) {
            if( i != c && (    // c darf sich selbst kennen
                !arr[i][c] ||  // Wenn Person i person c nicht kennt ist es nicht der Prominente
                arr[c][i] )    // Wenn der Person c person i kennt ist es auch nicht der Prominente
            ){
                return -1;
            }
        }
        return c;
    }
    /**
     *
     * HELPDESK:
     * Stack in Pseudocode?
     * Genau ein Prominenter? 
     * 
     * 
     * 
     *
     * Pseudocode:
     * marvin(Array F)
     * n <- length[F]
     * if (n = 1) do
     * | return 1
     * s <- new Stack()
     * for i <- 1 to n do
     * |  push(s, i)
     * while size(s) > 1 do
     * |  a <- pop(s)
     * |  b <- pop(s)
     * |  if F[a][b] do
     * |  |  push(s, a)
     * |  else do
     * |  | push(s, b)
     * return pop(s)
     *
     * 
     * Wir zeigen die Korrektheit des Algorithmus per Induktion und der Annahme das
     * genau ein Prominenter in der durch F repräsntierten Menge der Gäste ist:
     * IA:
     * n = 1
     * Das Array ist genau ein Element lang. Da mindestensten ein Prominenter vorhanden ist
     * wird in Zeile 3 der Index 1 zurückgegeben. Es gibt n-1 normale Gäste , also 1-1 = 0, und
     * nach Voraussetzung 1 Prominenten
     *
     * IV: Für alle n >= 1 gibt es genau einen Prominenten.
     * 
     * IS: n > 1
     * Zu beginn wird die Repräsentation aller Gäste der Gala einschließlich des Promimenten
     * auf einen Stack gelegt.
     * In der While-Schleife wird nun vom Stack zwei mal das oberste Element genommen.
     * Einmal Person A und einmal Person B. (Zeile 8 und 9).
     * In Zeile 10 wird geprüft ob Person A eine "kennt"-Beziehnung zu Person B führt.
     * 
     * Fall 1: Person A "kennt" Person "B":
     * Nach der Aufgabenstellung hat ein Prominenter die Eigenschaft keine der anderen
     * Personen zu kennen. Da Person A eine "kennt"-beziehung zu Person B führt ist dies ein Wiederspruch.
     * Person A kann nicht der Prominente sein.
     * Da Person B noch der Prominete sein kann wird Sie wieder auf den Stack gepackt. (Zeile 11)
     * Da beide Personen vom Stack entfernt wurden und nur die Person B wieder auf den Stack für weitere 
     * Prüfungen gelegt wurde, sind noch n-1 Gäste übrig.
     * 
     * 
     * Fall2: Person A "kennt" Person "B" nicht:
     * Nach der Aufgabenstellung kennt jeder Gast den Prominenten. Person B kann daher nicht der
     * Prominente sein.
     * Da Person A noch der Prominete sein kann wird Sie wieder auf den Stack gepackt. (Zeile 1)
     * Da beide Personen vom Stack entfernt wurden und nur die Person A wieder auf den Stack für weitere 
     * Prüfungen gelegt wurde, sind noch n-1 Gäste übrig.
     * 
     * Wenn nur noch eine Person auf dem Stack ist muss diese nach IV der Prominte sein, da alle anderen
     * Personen zuvor die Prominenteneigenschaft verletzt haben. Der Index des Promienten wird zurückgegeben.
     * 
     * 
     * Def Stack. Unser Stack entspricht dem Stack aus der Javaimplementierung.
     * Die Funktion size(s) nimmt eine Stack als Parameter und gibt die Anzhal an Elementen auf dem Stack zurück.
     * 
     * Die Funktion pop(s) nimmet einen Stack als Parameter entgegen. Es wird das oberste Element vom
     * Stack s entfert und als wert zurückgegeben.
     * 
     * Die Funktion push(s, a) nimmt als Parameter einen Stack und ein Element entgegen.
     * Das Element a wird als oberstes Element auf den Stack s gelegt.
     */
}