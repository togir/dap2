import java.util.Arrays;

public class aufgabe41 {
    public static void main(String[] args) {
        int[] arr = {4, 5, 2, 5, 1, 5, 6,7 ,2, 8 };
        System.out.println(minmalEnd(arr));
        System.out.println(SPT(arr));
        
    }



        public static int SPT (int[] L) {
            Arrays.sort(L);
            int sum = 0 ;
            int e = 0 ;
            for(int i = 0 ; i < L.length; i ++){
                e = e + L [i];
                sum = sum + e;
            }
            return sum ; 
    }




    private static int minmalEnd( int[] L) {
        Arrays.sort(L);
        int sum = 0;
        for (int i = 0; i < L.length; i++) {
            sum = sum + L[i];
        }
        return sum;
    }
}
/**
 * Guten Tag,
 * wir habe zwei Frage zu der Bewertung von Blatt 4.2 teil b und c
 * Zu Teil B:
 * Nach meiner Auffassung setzt die Aufgabenstellung nicht vorraus ein Array entgegenzunehmen.
 * Da dies auf den vorherigen Blättern auch nie der Fall war haben wir, wie es auch in der Vorlesung
 * gemacht wird, die Arrays A[] und D[] als global verfügbar definiert. Unser Algorithmus benötigt keine
 * For-Schleife, da er sich selbst rekursiv aufruft. Außerdem wird in der Aufgabenstellung keine benötigte
 * Schleife erwähnt oder eine Rekursion verboten. Zum globalen initialisierten Array, nach Absprache mit dem
 * Helpdesk wurde uns erlaubt für diese Aufgabe das Array A und D außerhalb der Methode zu initialiseren, solange
 * es über der Aufgabe erwähnt wird. Wir finden 0.5 / 2 Punkten für einen funktionierenden Algorithmus zu wenig.
 * 
 * Zu Teil C:
 * Wir können nicht nachvollziehen warum wir die Aussage (siehe kommentar im PDF) beweisen müssen,
 * da diese in der Musterlösung auch nicht explizit und ausführlich beweist. Außerdem ist die
 * Laufzeit unserer Funtkion O(n) und dies entspricht der Lösung der Musterlösung.
 *  
 *  Mit Freundlichen Grüßen
 */