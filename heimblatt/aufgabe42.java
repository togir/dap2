public class aufgabe42 {

    public static void main(String[] args) {
        int[][] D = {
            new int[6],
            new int[6]
        };
        
        System.out.println(D(5, 1, D) );
    }
    
    private static int D(int i, int j, int[][] D) {

        int[] A = {3, 7, 2, 1, 5, 8};
        //int[] A = {2,3,4,5,6};

        if(D[j][i] != 0){           // 1
            return D[j][i];         // 1
        }

        if ( (i == 0 ) && (j==0)){  // 1
            D[j][i] = 0 ;           // 1
            return D[j][i];         // 1
        }

        if(i == 0 && j == 1) {      // 1
            D[j][i] = A[0];         // 1
            return D[j][i];         // 1

        }

        if(i == 1 && j == 0) {      // 1
            D[j][i] = Math.max(D(0,0, D), D(0,1, D) );  // 1 + 2D
            return D[j][i];         // 1
        } 

        if(i == 1 && j == 1) {      // 1
            D[j][i] = Math.max(D(0,1, D), D(0,0, D)) + A[1]; // 1 + 2D
            return D[j][i];         // 1
        }

        if(i >= 1 && j == 0) {      // 1
            D[j][i] = Math.max(D(i-1, 1, D), D(i-1, 0, D)); // 3 + 2D
            return D[j][i];
        }

        if(i >= 1 && j == 1) {      // 1
            D[j][i] = Math.max((D(i-2, 0, D) + A[i-1] + A[i]), D(i-1, 0, D) + A[i]); // 7 + 2D
            return D[j][i];         // 1
        }

        return D[j][i];             // 1
    }
}
/**
 * Behauptung: Die maximale Anzahl an verkauften Autos,  
 * ist f ̈ur alle i≥1  durch D[i, j] gegeben.
 * 
 * IA: Sei i = 1
 *  Fall1: j = 0
 *  Da an diesem Tag pause gemacht wird werden 0 Autos verkauft D(1,0 ) = 0
 * 
 *  Fall2: j = 1
 *  An diesem Tag werden Autos verkauft. D(1, 0) = A[1..1]
 * 
 * 
 * IV: Sei i >= 1 und j = 0 o.B.d.A  dann berechnet D(i, j) die maximale Anzahl an verkaufen Autos.
 * 
 * IS: Fall1 :  j = 1 ; 
 *               D[j][i] = Math.max((D(i-2, 0) + A[i-1] + A[i]), D(i-1, 0,) + A[i]). In dieser Berechnung ist nach IV der erste Teil (D(i-2, 0, D) + A[i-1] + A[i])
 *               für die Möglichkeit da dass am Tag zuvor gearbeitet wurde und der hintere Teil nach IV D(i-1, 0, D) + A[i]) für die Möglichkeit dass am Tag zuvor 
 *               nicht gearbeitet wurde. Davon wird das maximum ermittelt und somit die bestmögliche variante für den heutigen tag. 
 * 
 *      Fall2: j = 0
 *          D[j][i] = Math.max(D(i-1, 1), D(i-1, 0)). Nach IV. ist D(i, j) die Maximale Anzahl an Verkauften Autos bis zum Tag i-1.
 *                  Durch den Maximalwert wird die maximale Anzahl an verkauften Autos vom Vortag, unabäning von j, ermittelt.
 *                  Da an Tag i nicht gearbeitet wird (j=0) stellt dies auch die maximale anzahl an verkauften Autos vom Tag i dar.
 * 
 */         


