import java.util.Arrays;

class aufgabe5 {
    
    public static void main(String[] args) {
        int k = 10;
        int[] A = {3, 2, 1, 1, 1, 1, 412, 1, 1, 1, 1};

        System.out.println("mj: " + halo(A, k));
        System.out.println("w:  " + m2(A.length-1, k, A));
        System.out.println("w neu :" + m3(2, k, A));
    }
    //                                                                  j = index mit dem das Teilfeld endet
    //                                                                  k = maximale summe im Teilfeld die erreicht werden darf
    //                                                                  A = Array
    private static int m3(int j, int k, int[] A) {  //                  m(j, k, A)
        int n = A.length;                           // 1                n <- length(A)
        int[][] B = new int[n][n];                  // 1                B <- B[1..n][1..n]
        

        for(int i = 0; i < n; i++) {                // n                for i <- 1 to n do:
            int sum = 0;                            // 1                | sum <- 0
            int z = i;                              // 1                | z <- i 
            while(z < n && sum + A[z] <= k) {       // n                | while z<n and sum + A[z] <= k do 
                sum = sum + A[z];                   // 1                | | sum <- sum + A[z]
                if(z == 0) {                        // 1                | | if z = 0 then do 
                    B[i][z] = 1;                    // 1                | | | B[i][z] = 1 
                } else {                            // 1                | | else
                    B[i][z] = B[i][z-1] + 1;        // 1                | | | B[i][z] = B[i][z-1]+1
                }                                                 //    | | z = z + 1
                z = z+1;                            // 1                return max(B[1...n][j])
            }
        }

        for (int i = 0; i < B.length; i++) {
            System.out.print(A[i] + " ");
            System.out.print(Arrays.toString(B[i]));
            System.out.println();

        }
        //max(B[1...n][j])                          // n
        int max = 0;
        for (int i = 0; i < n; i++) {
            max = Math.max(B[i][j], max);
        }

        return max;
    }
    /**
     * Die for-schleife wird n mal aufgerufen.
     * Die in der Forschleife enthaltene While-Schleife hat bei jeder erhöhung von i eine 
     * durchlauf weniger hat allerdings im ersten fall n durchläufe also auch eine laufzeit von SUM_{i=0}^n (n-i);
     * Insgesamt kommt n +  n *  SUM_{i=0}^n (n-i) = n + n*n + n*(n-1) + n*(n-2) + n*(n-3) + n*(n-4) ... element von O(n²).
     */

    private static int m2(int j, int k, int[] A) {
        int counter = 0;
        int sum = 0;

        for(int i = j; i >= 0; i--) {
            sum = sum + A[i];
            if(sum <= k) {
                counter = counter +1;
            } else {
                return counter;
            }
        }
        return counter;
    }
    // Anstatt jedes mal zu überprüfen ob das Feld an Index i noch gülltig ist
    // Speichern wir summe von Index i+1 bis j damit wir Rechenschrite Sparen

    public static int halo(int[] A , int k) {
        int n = A.length;
        
        int[][] M = new int [n][k+1];
        
        for (int i = 0; i < n; i++) {
            M[i][0] = 0 ;
        }

        for (int p = 0; p < k; p++) {
            if(A[1]>p){
                M[1][p] = 0;
            }
            else{
                M[1][p] = 1;
            }
        }

        for (int i = 1; i < n; i++) {
            for (int p = 0; p < k ; p++) {
                if(A[i] > k ){
                    M[i][p] = 0 ;
                }
                else{
                    if(p-A[i] >= 0){
                        M[i][p] = M[i-1][p-A[i]] +1;
                    }
                    else{
                        M[i][p] = M[i-1][A[i]-p];
                    }
                }
            }
        }
        int max = 0;

        

        for(int i=0; i< k; i++){            
            for(int j=i;j<k;j++) {         
                if(M[i][j] > max){            
                    max = M[i][j];            
                }
            }
        }
        return max;
    }

    private static int m(int j, int k, int[] A) {

        int[] B = new int[j];
        int p = 0;
        for (int i = 0; i < j; i++) {
            
            if(A[i]> k) {
                B[i] = 0;
                p = 0;
            } else {

            

                if(A[i]+p <= k && i > 0){
                    p = A[i] + p;
                    B[i] = B[i-1] + 1;

                }
                if(A[i]+p > k && i > 0){
                    B[i] = 0;
                    p = 0;
                }
                if(A[i]+p > k && i == 0){
                    B[i] = 0;
                    p = 0;
                }
                if(A[i]+p <= k && i == 0){
                    p = A[i] + p;
                    B[i] = 1 ; 

                }
            }
            System.out.println(Arrays.toString(B)); 
        }
        int max = 0;
        for (int i = 0; i < B.length; i++) {
            max = Math.max(B[i], max);
        }

        return max;
        
    }
}

/**
 * Laufzeit
 * 13 + i, da i <= n O(n)
 */

/**
 * Beweisen Sie die Korrektheit Ihrer rekursiven Form.
 * 
 * Fall A[i] > k
 * Da das Teilfeld mit A[i] enden muss kann man kein Positive
 * Zahl die , möglicherweise, davor liegt dazuaddieren um sum(A[..i]) <= k
 * zu erreichen. Es wird 0 zur Anzahl der passenden Indizes dazugezählt und
 * es geschieht der Abbruch der Funktion
 * 
 * Fall i == 0 und A[i]+p > k
 * Dies ist eine Abbruchbedingung, da i = 0 kann unser Teilfeld nicht größer werden da 
 * wir am ersten Index unseres Feldes sind. In diesem Fall überschreitet der Wert A[i] addiert
 * mit der deerzeitigen Summe die Grenze k und i wird nicht in die größtmögliche Teilfolge miteinbezogen.  
 * 
 * Fall i == 0 und A[i]+p < k
 * Dies ist eine Abbruchbedingung, da i = 0 sind wir am Anfang des Arrays unser Teilfeld kann
 * nicht mehr größer werden. Da die Summe von A[i] + p, wobei p die Summe des restlichen Teilfeldes 
 * ist, kleiner als k ist wird das element A[i] noch zur Teilliste dazugezählt.
 * Es wird 1 zurückgegben um die Länge um 1 zu erhöhen.
 * 
 * Fall i > 0 && A[i] + p <= k:
 * Dies ist der Fall wenn A[i] zu der Summe dazu genommen werden kann ohne, dass die Grenze k 
 * überschritten wird. Es folgt der rekursive Aufruf von m() mit i-1 da die größtmögliche Teil-
 * folge noch weiter gehen könnte mit +1 ,da der Wert von A[i] noch in die Summe passt. P, welches
 * die Summe aus dem Teilfeld von i bis zum jetzigen Aufruf ist ,  wird für
 * den nächsten Aufruf zu p + A[i] , da A[i] + 1 nicht > k ist . 
 *
 *
 * Fall i > 0 && A[i] + p > k:
 *  Die ist eine Abbruchbedingung, da die Summe A[i] + p, wobei p die Summe des restlichen Teilfeldes 
 * ist, größer als k ist. Deswegen wird auch das Element A[i] nicht mehr zum Teilfeld hinzugenommen.
 * Es wird 0 zurückgegeben, da die Länge nicht vergrößert wurde,
 */