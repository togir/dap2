import java.util.Arrays;

class aufgabe52 {
    public static void main(String[] args) {
        int[] arr = {1,10,1,4,1,2,1};
        int[] barr = new int[arr.length];
        Arrays.fill(barr, Integer.MAX_VALUE);
        //System.out.println(g(7,barr,arr));
        System.out.println(g2(7, arr));
    }

    private static int g2(int i, int[] A) {
        int [] B = new int[i];
        Arrays.fill(B, Integer.MAX_VALUE);
        B[0] = 0;

        // Für jedes Element in A[] bis zum Index i werden die Anzahl an notwendigen schritten
        // errechtnet
        for (int j = 0; j < i; j++) {

            int aktuellerWert = B[j];

            // For-schleife die alle erreibaren Felder füllt
            for (int z = 1; z <= A[j]; z++) {
                // Die von A[j] aus erreichbaren Felder auf die Sprunganzahl B[j] +1 setzen
                // auser es steht schon ein kleinere Wert im Feld.
                // Wenn wir über die Arraygrenze gehen soll nichts eingefügt werden
                if(j +z < i) {
                    B[j+z] = Math.min(aktuellerWert +1, B[j+z]);
                }
            }
        }

        return B[i-1];

    }
    /**
     * i = Index der erreicht werden soll
     * A = Array mit den Sprunganzahlen 
     * g(i , A)
     * 
     * B <-[1..i] // 1
     * B[1..i] <- infinity // n
     * 
     * for j <- 1 to i do: // n
     * |    c <- B[j] // 1
     * |    for z <- 1 to A[j] do: // k
     * |    |   if j+z < i // 2
     * |    |   |   B[j +z] <- min(c+1, B[j+z]) // 4
     * return B[i] // 1
     * 
     * Laufzeit:
     * Das füllen des Arrays mit unendlich hat die Laufzeit n
     * Die erste For-Schleife wir i mal aufgerufen.
     * Die Forschleife im Inneren wird abhängig vom wert in A[j] aufgerufen. Wir nehmen für diesen Wert ein maximum von k an.
     * Die gesamte Laufzeit beträgt dann n + n*(k + 6) + 2 element O(n²) da k größer gleich n sein kann
     *      
    */
    private static int g(int i , int[] G, int[] A){
        System.out.println(Arrays.toString(G));

        if(G[i] != Integer.MAX_VALUE) {         // 1
            return G[i];                        // 1
        }
        
        if(i == 0) {                            // 1
            G[i] = 0 ;                          // 1
            return 0;                           // 1
        }

        int min = Integer.MAX_VALUE;            // 1
        for (int j = 0; j < i; j++) {           // 1
            if(A[j] + j >= i) {                 // 2
                min = Math.min(g(j, G, A), min);// n - 1 + 1
            }
        }

        G[i] = min +1;                          // 1
        return G[i];                            // 1


    } //d)
    // Laugezeit: 11 + n = O(n)
    // Wir haben in $zeile die Laufzeit als n gewählt. Die funktion wird zwar öfters als n-mal
    // aufgerufen aber da die Werte nach dem erstem Aufruf in Array G gespeichert werden sind
    // haben alle nachfolgenen Aufrufe eine Laufzeit von 2 (siehe Zeile 15-16).
}
/**c)

 * g(i)
 * if g[i] !=  unendlich then do 
 * |     return g[i]
 * if i = 1 then do 
 * |     g[i] = 0
 * min = unendlich
 * for j = 1 to i - 1 do
 * |    if A[j] + j >= i do 
 * |    |   min = min ( g(j), min)
 * g[i] = min +1
 * return g[i]      
 */
/** b )
 *  
 *  Fall i == 1:
 *  Dies ist die Abbruchbedingung. Da wir am Anfang des Arrays sind
 *  geben wir 0 als Anzhal der Schritte zurück. Dies definiert sich aus der 
 *  Aufgabestellung da am index 1 gestartet wird und dementsprechen noch
 *  keine Schritte gemacht wurden.
 * 
 * 
 * Fall i > 1:
 * 
 * Wir bestimmen die Menge aller g(j) von denen aus wir unseren aktuellen Index
 * i in einem Schritt erreichen können. J muss dabei kleiner als i sein. Um zu bestimmen ob wir 
 * A[i] von A[j] erreichen könnnen wird der Index j mit dem Wert A[j] summiert. Ist diese Ergebniss
 * größer gleich i so ist das Element A[i] vom Element A[j] aus in einem Schritt erreichbar.
 * 
 * Von der bestimmten Menge aus g(j) wird zusammen mit unendlich das Minimum gebildet. 
 * Nach Aufgabenstellung wird unendlich in dem Fall benutzt , indem es keine Möglichkeit gibt
 * zu unserem Element A[i] zu gelangen. In diesem Fall ist die menge der g(j) leer, daher wird
 * in dem Minimum "unendlich" gewählt.
 * Am Ende zählen wir die Anzahl der Schritte +1 um den Schritt von dem gewählten A[j] auf A[i] mitzuzählen.
 * 
 */