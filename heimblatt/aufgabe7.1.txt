
Invariante: Elemente (x_1 x_2) <- schon im Baum    (y_1 y_2) <- wird eingefügt
- Wenn  der Baum Leer ist Element einfügen. Fall(1)

- wenn x_1 >= als y_1 und x_2 >= y_2    Fall(2)
    (y_1 y_2) in linken Teilbaum einfügen()

- wenn x_1 < als y_1 und x_2 >= y_2     Fall(3)
    (y_1 y_2) in rechten Teilbaum einfügen()

- wenn x_1 < als y_1 und x_2 < y_2         Fall(4)
    (y_1 y_2) als wurzel nehmen und (Teilbaum von) (x_1, x_2) links an (y_1, y_2) einfügen()
    Die Kinder von (x_1, x_2) werden aus dem Baum gelöscht alle an der neuen Wurzel (y_1 y_2) neu eingefügt.

- wenn x_1 >= als y_1 und x_2 < y_2     Fall(5)
    (y_1 y_2) als Wurzel nehmen und (Teilbaum von) (x_1, x_2) rechts an (y_1, y_2) einfügen
    Die Kinder von (x_1, x_2) werden aus dem Baum gelöscht alle an der neuen Wurzel (y_1 y_2) neu eingefügt.

    Anhand Beispiel :
[(2,40),(1,10),(3,80),(8,20),(1,50)]

Wir fügen (2, 40) ein. Der Baum ist noch leer deshalb
wird (2, 40) als neuen Wurzel eingefügt. Es folgt der Fall(1).

Wir fügen (1, 10) ein. Da 2 >= 1 ist und 40 >= 10 ist
wird das Element in den linken Teilbaum von (2, 40) eingefügt. Es folgt der Fall(2).
Da dieser Teilbaum leer ist wir (1, 10) links an (2, 40) angehängt. Es folgt der Fall (1).

Wir fügen (3, 80) ein. Da 2 < 3 und 40 < 80 ist folgt Fall (4).
(3,80) wird die neue Wurzel. (2, 40) wird links an (3, 80) angehängt.
(1, 10) wird an (3, 80) neu eingefügt. Es landet nach fall(2) links unter (2, 40)

Wir fügen (8, 20) ein. Da 3 < 8 und 80 >= 20 ist. Folgt fall(3).
Das Element (8, 20) wird rechts an (3, 80) eingefügt. 

Wir fügen (1, 50) ein. 
Da 3 >= 1 und 80 >= 50 wird die einfügen Operation rekusiv auf (2, 40) aufgerufen. Es folgt Fall (2).

Dann wird (1,50) mit (2, 40) verglichen. Da 2 >= 1 und 40 < 50 ist folgt Fall (5).
(1, 50) wird demnach die Wurzel des linken Teilbaumes von (3, 80) und (2, 40) wird 
rechts an (1, 50) eingefügt. Die Kinder von (2, 40) werden aus dem Baum entfernt und an 
(1, 50) neu eingefügt. Daraus ergibt nach Fall(2) sich, dass (1, 10) links an (1, 50) eingefügt wird.
