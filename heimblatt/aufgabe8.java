package heimblatt;
import java.util.*;
/**
 * aufgabe8.2
 */
public class aufgabe8 {
    public static void main(String[] args) {
        int [] A = new int[100];
        int n = 99 ;
        System.out.println(fuc2(n));
        //System.out.println(func ( A , n)); 
        
    }
    

    public static int fuc2 (int n) {

        int[] A = new int[n];

        A[0] = 0;
        A[1] = 0;

        for (int i = 2; i < A.length; i++) {
            int k = 10 * (int)Math.sin(i) + 20;
            A[i] = A[(int)Math.pow(2, (int) Math.log(i-1))] + A[(int)Math.sqrt(i)] + k;
        }

        return A[n-1];
    }
}
