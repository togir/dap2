import math

n = 9

A = [0] * hash(n);

def main():
    
    function(n)
    print (function(n))
    print(funcRek(n))

def function(n):

    # print("Eingabe: ", n);
    
    if (n == 1 or n == 0):
        return 0
    
    if(math.log2(n) == math.floor(math.log2(n))):
        if A[hash(n)] != 0:
            print("Wert ", n ," ist in der Hashtable")
            print("--------------------")

            return A[hash(n)]

        
        A[hash(n)] = function(2**math.floor(math.log(n-1, 2))) + function(math.floor(math.sqrt(n))) + math.floor(10 * math.sin(n) +20)
        print("Wert ", n ," wurde neu berechnet")
        print("--------------------")
        return A[hash(n)]


    print("Wert ", n ," wurde neu berechnet und nicht gespeichert")
    print("--------------------")
    return function(2**math.floor(math.log(n-1, 2))) + function(math.floor(math.sqrt(n))) + math.floor(10 * math.sin(n) +20)

    

def hash(x):
    return math.floor(math.log2(x))
    

def funcRek(n):

    if (n == 1 or n == 0):
        return 0

    value = funcRek(2**math.floor(math.log(n-1, 2))) + funcRek(math.floor(math.sqrt(n))) + math.floor(10 * math.sin(n) +20)
    print("Wert ", n ," wurde berechnet und nicht gespeichert")
    print("--------------------")
    return value


main()
'''
Wert  9  wurde neu berechnet und nicht gespeichert
--------------------
Wert  2  wurde neu berechnet
--------------------
Wert  2  ist in der Hashtable
--------------------
Wert  4  wurde neu berechnet
--------------------
Wert  2  ist in der Hashtable
--------------------
Wert  8  wurde neu berechnet
--------------------
Wert  3  wurde neu berechnet und nicht gespeichert
--------------------
Wert  2  ist in der Hashtable
--------------------
128

Wert  2  wurde berechnet und nicht gespeichert
--------------------
Wert  2  wurde berechnet und nicht gespeichert
--------------------
Wert  4  wurde berechnet und nicht gespeichert
--------------------
Wert  2  wurde berechnet und nicht gespeichert
--------------------
Wert  8  wurde berechnet und nicht gespeichert
--------------------
Wert  2  wurde berechnet und nicht gespeichert
--------------------
Wert  3  wurde berechnet und nicht gespeichert
--------------------
Wert  9  wurde berechnet und nicht gespeichert
--------------------
202


if eingabe ist zweierpotenz:
    wert in table speichern wenn noch nicht da
else
    rekursiver Aufruf ohne Speicherung



erste teil der Funktion ruft nur 2er Potzenzen auf max O(log n) nach dem ersten aufruf sind
diese Zwischengespeichert. Ein erneuter aufruf erfolgt daher in Konstanter Zeit.

=> O(log n) + c = O( log n)

Der Zweite Teil der Funktion fällt steiler als der erste Teil. Daher erzeugt er weniger Aufrufe als
der erste Teil. => kleinergleich O(log n)


Der 3.Teil der Funktion hat eine Konstannte Laufzeit O(1)

==>
(O(n) * O(n) ) + O(1) } € O(logn^2)
'''