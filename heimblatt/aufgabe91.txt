  WK-Algorithmus(Datenstrom a (a1,...,an))
1.  s <- /0 (als Stack)    
2.  for i <- 1 to n    
3.    |    if size(s) = 0 
4.    |      |  push (ai)
5.    |    else  
6.    |      |  a <- pop(s)
7.    |      |  b <- ai
8.    |      |  if a != b 
9.    |      |   |  Lösche a und b(tu nichts)
10    |      |  else 
11.   |      |   |   push(a, s)
12.   |      |   |   push(b, s)
13.   c <- pop(s)
14.   return c 

b) 
Nein.

Bsp.: Sei a ein Datenstrom mit n > 7 Elementen. Der Datenstrom ist so gewählt das alle Elemente
a_0.. a_n -3 für gerade idizes gleich sind und alle Elemente mit ungeraden Indizes gleich sind. 

Die Elemente a_n-2, a_n-1, a_n sind gleich. Somit ist der Stack spätestens beim Element a_n-2 leer. 
a_n-1 wird auf den Stack gelegt. 
a_n ist gleich a_n-1 deswegen werden beide zurück auf den Stack gelegt.
nun ist auf dem Stack in Zeile 13 das Element a_n. Der Algorithmus wird a_n zurückgeben obwohl
dieses Element nicht das Mehrheitselement darstellt. Es lässt sich auch keine Aussage über die Häfigkeit von
den Elementen a_n-2 ... a_n treffen.


d)
Antwort: 0

{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}

In Zeile 13 des Algorithmus wird die leere Menge zurückgeben, da alle Elemente einen count
von 1 < 0.5*16/2 => 1 < 16/4 => 1 < 4.

e)
Nein die Reihenfolge beeinflusst den Algorithmus nicht.

Sei a ein Datenstrom mit der Länge n. Und die ersten n/4 Elemente entsprechen a_1 , die zweiten n/4 Elemente a_2 , die dritten Elemente a_3 und die letzten a_4.
Wenn man die Reihenfolge der einzufügenden Knoten ändert , dann wird weiterhin keine Ausgabe getätigt , da alle einen count <= n/4 haben.
Um einer Count größer als n/4 zu erreichen muss ein Element mehr als n/4 mal im Array vorkommen. Da am ende des Streams
die 4 Häufigstens Elemente im Baum stehen ist es egal in welcher Reihenfolge die Elemente im Stream sind.